import React from "react";
import ReactDOM from "react-dom";
import App from "./app/App";
import * as serviceWorker from "./serviceWorker";

import "./style/tailwind.css";

import "./style/style.scss";
import "./style/mainPage.scss";
import "./style/userPage.scss";
import "./style/signInPage.scss";
import "./style/product.scss";
import "./style/productList.scss";
import "./style/productDetail.scss";

import "./app/firebase/firebase";

ReactDOM.render(<App />, document.getElementById("root"));

serviceWorker.unregister();
