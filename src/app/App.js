import React, { Suspense } from "react";
import { Route, BrowserRouter } from "react-router-dom";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Provider from "react-redux/es/components/Provider";
import Loadable from "react-loadable";
import history from "../@history";
import store from "./store";

import { routes } from "./configs/routes";
import Loader from "./layout/Loader";
import Auth from "./auth";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#394b59",
    },
    text: {
      primary: "#0d0c22",
    },
  },
  overrides: {
    MuiButton: {
      root: {
        textTransform: "none",
      },
    },
  },
  props: {
    MuiButton: {
      variant: "contained",
      size: "small",
      className: "mr-2",
      color: "primary",
    },
    MuiTextField: {
      variant: "outlined",
      size: "small",
    },
  },
});

const MainLayout = Loadable({
  loader: () => import("./layout"),
  loading: Loader,
});

const AppContext = React.createContext({});

const App = () => {
  return (
    <AppContext.Provider value={{ routes }}>
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <Auth>
            <BrowserRouter history={history}>
              <Suspense fallback={<Loader />}>
                <Route path="/" component={MainLayout} />
              </Suspense>
            </BrowserRouter>
          </Auth>
        </Provider>
      </ThemeProvider>
    </AppContext.Provider>
  );
};

export default App;
