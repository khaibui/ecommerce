import React, { Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import withReducer from "../store/withReducer";
import reducer from "../utils/store/reducer";
import Loader from "./Loader";
import ScrollToTop from "./ScrollToTop";
import { routes, needSingIn, adminRoutes } from "../configs/routes";
import { ADMIN_ACCOUNT } from "../utils/Constraint";

import AdminNavbar from "../components/AdminNavbar";
import Navbar from "../components/Navbar";

const MainLayout = () => {
  const userStatus = useSelector(({ User }) => User.User.userStatus);

  const renderRoute = () => {
    if (userStatus) {
      if (ADMIN_ACCOUNT?.includes(userStatus.email)) {
        return [
          ...adminRoutes.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              name={route.name}
              render={(props) => <route.component {...props} />}
            />
          )),
          <Redirect to="/" />,
        ];
      } else if (userStatus.email) {
        return [
          ...routes.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              name={route.name}
              render={(props) => <route.component {...props} />}
            />
          )),
          <Redirect to="/" />,
        ];
      }
    }
    const accessRoutes = needSingIn.map((route, index) => (
      <Route
        key={index}
        path={route.path}
        exact={route.exact}
        name={route.name}
        render={(props) => <route.component {...props} />}
      />
    ));
    return [...accessRoutes, <Redirect to="/sign-in" />];
  };

  return (
    <div>
      <Suspense fallback={<Loader />}>
        <ScrollToTop />
        {ADMIN_ACCOUNT?.includes(userStatus?.email) ? (
          <AdminNavbar />
        ) : (
          <Navbar />
        )}
        <Switch>{renderRoute()}</Switch>
      </Suspense>
    </div>
  );
};

export default withReducer("User", reducer)(MainLayout);
