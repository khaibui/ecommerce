import React from "react";
import $ from "jquery";

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const Error404 = React.lazy(() => import("../pages/ErrorPage/Error404"));
const MainPage = React.lazy(() => import("../pages/MainPage/MainPage"));
const SignInPage = React.lazy(() => import("../pages/SignInPage/SignInPage"));
const SignUpPage = React.lazy(() => import("../pages/SignUpPage/SignUpPage"));
const ProductPage = React.lazy(() =>
  import("../pages/ProductPage/ProductPage")
);
const ProductDetailPage = React.lazy(() =>
  import("../pages/ProductDetailPage/ProductDetailPage")
);
const UserPage = React.lazy(() => import("../pages/UserPage/User"));

// Admin route
const AdminPage = React.lazy(() => import("../pages/AdminPage"));
const Analysis = React.lazy(() => import("../pages/AdminPage/Analysis"));
const UserManage = React.lazy(() => import("../pages/AdminPage/UserManage"));
const ProductManage = React.lazy(() =>
  import("../pages/AdminPage/ProductManage")
);
const ProcessManage = React.lazy(() =>
  import("../pages/AdminPage/ProcessManage")
);

export const routes = [
  {
    path: "/",
    exact: true,
    name: "MainPage",
    component: MainPage,
  },
  {
    path: "/product/:category?",
    exact: true,
    name: "ProductPage",
    component: ProductPage,
  },
  {
    path: "/product-detail/:category/:product?",
    exact: true,
    name: "ProductDetailPage",
    component: ProductDetailPage,
  },
  {
    path: "/user/:page",
    exact: true,
    name: "UserPage",
    component: UserPage,
  },
  {
    path: "/404",
    exact: true,
    name: "Error",
    component: Error404,
  },
];

export const needSingIn = [
  {
    path: "/",
    exact: true,
    name: "MainPage",
    component: MainPage,
  },
  {
    path: "/sign-in",
    exact: true,
    name: "SignInPage",
    component: SignInPage,
  },
  {
    path: "/sign-up",
    exact: true,
    name: "SignUpPage",
    component: SignUpPage,
  },
  {
    path: "/product/:category?",
    exact: true,
    name: "ProductPage",
    component: ProductPage,
  },
  {
    path: "/product-detail/:category/:product?",
    exact: true,
    name: "ProductDetailPage",
    component: ProductDetailPage,
  },
];

export const adminRoutes = [
  {
    path: "/",
    exact: true,
    name: "AdminPage",
    component: UserManage,
  },
  {
    path: "/analysis",
    exact: true,
    name: "Analysis",
    component: Analysis,
  },
  {
    path: "/product-manage",
    exact: true,
    name: "ProductManage",
    component: ProductManage,
  },
  {
    path: "/process-manage",
    exact: true,
    name: "ProcessManage",
    component: ProcessManage,
  },
];
