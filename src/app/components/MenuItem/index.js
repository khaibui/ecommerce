import { MenuItem } from "@blueprintjs/core";

const MenuItemCustom = (props) => {
  return (
    <MenuItem
      {...props}
      value={props.value}
      className="rounded-lg menu-item"
      icon={props.icon}
      text={<div value={props.value}>{props.text}</div>}
    />
  );
};

export default MenuItemCustom;
