import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

import ProductItem from "./ProductItem";

const ProductList = (props) => {
  const history = useHistory();
  const ITEM_PER_PAGE = 5;
  const totalItemRow = ITEM_PER_PAGE * props.moreFactor;

  const productData = useSelector(({ User }) => User.User.products);

  const [productList, setProductList] = useState([]);

  const onItemClick = (productParam) =>
    history.push(
      `/product-detail/${productParam.cateCode}/${productParam.productCode}`
    );

  const isItemIncludeInString = (arrayParam, textParam) =>
    arrayParam.some((item) =>
      textParam.toLowerCase().includes(item.toLowerCase())
    );

  const filterAction = (filterParam, productParam) => {
    const { byCate, byRam, byQud, bySize, byCpu } = filterParam;
    return productParam.filter((item) => {
      const byCateValid =
        !byCate.length || isItemIncludeInString(byCate, item.cateCode);
      const byRamValid =
        !byRam.length || isItemIncludeInString(byRam, item.productDetail.ram);
      const byQudValid =
        !byQud.length ||
        isItemIncludeInString(byQud, item.productDetail.display);
      const bySizeValid =
        !bySize.length ||
        isItemIncludeInString(bySize, item.productDetail.display);
      const byCpuValid =
        !byCpu.length || isItemIncludeInString(byCpu, item.productDetail.cpu);
      console.log(byCateValid, byRamValid, byQudValid, bySizeValid, byCpuValid);
      return (
        byCateValid && byRamValid && byQudValid && bySizeValid && byCpuValid
      );
    });
  };

  const sorterAction = (sorterParam, productParam) => {
    const { sorter, priceScale } = sorterParam;
    const filterByPriceScale = productParam.filter((item) => {
      const itemPrice =
        item.productPrice * ((100 - item.productDiscount) / 100);
      return itemPrice >= priceScale[0] && itemPrice <= priceScale[1] * 1000000;
    });
    switch (sorter) {
      case "discount":
        return filterByPriceScale.sort(
          (a, b) => b.productDiscount - a.productDiscount
        );
      case "increase":
        return filterByPriceScale.sort(
          (a, b) => a.productPrice - b.productPrice
        );
      case "decrease":
        return filterByPriceScale.sort(
          (a, b) => b.productPrice - a.productPrice
        );
      default:
        return filterByPriceScale;
    }
  };

  useEffect(() => {
    const filterProductData = props.filter
      ? filterAction(props.filter, productData)
      : productData;
    const sorterProductData = props.sorter
      ? sorterAction(props.sorter, filterProductData)
      : filterProductData;
    setProductList(sorterProductData);
  }, [props.filter, props.sorter, productData]);

  return (
    <div {...props} className="product-sort-container">
      {productList.length ? (
        <div>
          {[
            ...Array(
              totalItemRow <= productList.length
                ? totalItemRow
                : productList.length
            ).keys(),
          ].map((pgId) => (
            <div className="product-list-horizontal">
              {productList.slice(pgId * 5, (pgId + 1) * 5).map((item) => (
                <ProductItem onClick={() => onItemClick(item)} product={item} />
              ))}
            </div>
          ))}
        </div>
      ) : (
        <div className="user-sale-container" style={{ width: "100%" }}>
          <img src={process.env.PUBLIC_URL + "/assets/no-product.png"} />
        </div>
      )}
    </div>
  );
};

export default ProductList;
