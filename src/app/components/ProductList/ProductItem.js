import React, { useState } from "react";

const ProductItem = (props) => {
  const productItemData = props.product;

  const Title = (props) => {
    const titleFull = `${props.data.productTitle} ${props.data.productTitleScript}`;
    return (
      <div className="product-list-item-title">
        {titleFull.length < 70 ? titleFull : `${titleFull.slice(0, 70)} ...`}
      </div>
    );
  };

  const Price = (props) => {
    const originPrice = props.data.productPrice;
    const hasDiscount = props.data.productDiscount;
    // const hasFreeShip = props.data.productFreeShip;
    const finalPrice =
      hasDiscount > 0 ? originPrice * (1 - hasDiscount / 100) : originPrice;
    return (
      <div>
        <div className="product-list-item-price-containter">
          <div className="product-list-item-price">
            {`${finalPrice.toLocaleString()} đ`}
          </div>
          <div className="product-list-item-freeship">
            <img
              className="product-list-item-freeship-icon"
              src={process.env.PUBLIC_URL + "/assets/free-ship.png"}
            />
          </div>
        </div>
        {hasDiscount > 0 && (
          <div className="product-list-item-discount-container">
            <p className="product-list-item-origin-price">{`${originPrice.toLocaleString()} đ`}</p>
            <p className="product-list-item-discount">{`${hasDiscount} %`}</p>
          </div>
        )}
      </div>
    );
  };

  return (
    <div className="product-list-item-container" {...props}>
      <img
        className="product-list-item-image"
        // src={process.env.PUBLIC_URL + "/products/1.png"}
        src={productItemData.image[0]}
      />
      <div>
        <Title data={productItemData} />
        <Price data={productItemData} />
      </div>
    </div>
  );
};

export default ProductItem;
