import React from "react";
import { useHistory, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Button, Navbar, NavbarGroup } from "@blueprintjs/core";

import ConfirmDialog from "../../components/ConfirmDialog";

import * as FirebaseService from "../../firebase/service";
import * as Actions from "../../utils/store/action";

const NavbarComponent = () => {
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  const userStatus = useSelector(({ User }) => User.User.userStatus);

  const onOptionClick = (route) => history.push(`/${route}`);

  const onLogoutClick = () => {
    dispatch(
      Actions.openDialog(
        "Xác nhận đăng xuất",
        "Bạn có muốn đăng xuất khỏi tài khoản.",
        () => {
          FirebaseService.signOut();
          history.push("/");
        },
        "log-out"
      )
    );
  };

  return (
    <React.Fragment>
      <ConfirmDialog />
      <Navbar className="bp3-dark navbar-container" fixedToTop>
        <NavbarGroup align="right">
          <Button
            minimal
            icon="shared-filter"
            text="Quản lý tài khoản"
            onClick={() => onOptionClick("")}
            active={location.pathname === "/"}
          />
          <Button
            minimal
            icon="barcode"
            text="Quản lý sản phẩm"
            onClick={() => onOptionClick("product-manage")}
            active={location.pathname === "/product-manage"}
          />
          <Button
            minimal
            icon="cycle"
            text="Quản lý đơn vận"
            onClick={() => onOptionClick("process-manage")}
            active={location.pathname === "/process-manage"}
          />
          <Button
            minimal
            icon="timeline-area-chart"
            text="Thống kê sản phẩm"
            onClick={() => onOptionClick("analysis")}
            active={location.pathname === "/analysis"}
          />
          <span class="bp3-navbar-divider"></span>
          <Button minimal icon="user" text={userStatus?.email || "Anonymous"} />
          <Button
            minimal
            icon="log-out"
            text="Đăng xuất"
            onClick={onLogoutClick}
          />
        </NavbarGroup>
      </Navbar>
    </React.Fragment>
  );
};

export default NavbarComponent;
