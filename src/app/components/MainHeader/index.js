import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { Button } from "@blueprintjs/core";
import { Tooltip2 } from "@blueprintjs/popover2";
import { Omnibar } from "@blueprintjs/select";
import { Badge, IconButton } from "@material-ui/core";
import {
  ShoppingCartOutlined,
  AccountCircleOutlined,
  LocalOfferOutlined,
  TurnedInNotOutlined,
  Search,
} from "@material-ui/icons";

const MainHeader = () => {
  const history = useHistory();

  const userStatus = useSelector(({ User }) => User.User.userStatus);
  const userData = useSelector(({ User }) => User.User.user);

  const iconProps = { fontSize: "large", color: "disabled" };

  const [searchOpen, setSearchOpen] = useState(false);

  const onSearchClick = () => setSearchOpen(true);
  const onLogoClick = () => history.push("/");
  const onCartClick = () => history.push("/user/cart");
  const onBillClick = () => history.push("/user/wait");
  const onSignInClick = () =>
    userStatus ? history.push("/user/profile") : history.push("/sign-in");

  return (
    <div className="main-header-container">
      <img
        className="main-header-logo"
        src={process.env.PUBLIC_URL + "/assets/3.png"}
        onClick={onLogoClick}
      />
      <div>
        <Tooltip2 placement="bottom" content="Tìm kiếm">
          <IconButton aria-label="search" onClick={onSearchClick}>
            <Search {...iconProps} />
          </IconButton>
        </Tooltip2>
        <Tooltip2 placement="bottom" content="Đơn hàng">
          <IconButton aria-label="user" onClick={onBillClick}>
            <TurnedInNotOutlined {...iconProps} />
          </IconButton>
        </Tooltip2>
        <Tooltip2
          placement="bottom"
          content={userStatus ? userStatus.email : "Đăng nhập"}
        >
          <IconButton aria-label="user" onClick={onSignInClick}>
            <AccountCircleOutlined {...iconProps} />
          </IconButton>
        </Tooltip2>
        <Tooltip2 placement="bottom" content="Giỏ hàng">
          <IconButton aria-label="cart" onClick={onCartClick}>
            <Badge
              badgeContent={
                userData && Array.isArray(userData.userCart)
                  ? userData.userCart.length
                  : 0
              }
              color="secondary"
            >
              <ShoppingCartOutlined {...iconProps} />
            </Badge>
          </IconButton>
        </Tooltip2>
        <Omnibar
          allowCreate={false}
          isOpen={searchOpen}
          resetOnSelect
          items={[]}
          onClose={() => setSearchOpen(false)}
          inputProps={{
            placeholder: "Nhập từ khóa tìm kiếm",
            leftElement: <Button icon="search" minimal />,
          }}
          onQueryChange={(e) => console.log(e)}
        />
      </div>
    </div>
  );
};

export default MainHeader;
