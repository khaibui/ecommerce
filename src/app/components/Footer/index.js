import React from "react";
import {
  Alignment,
  AnchorButton,
  Button,
  ButtonGroup,
  H5,
  Switch,
} from "@blueprintjs/core";

import "../../../style/component/footer.scss";

export default function Footer() {
  return (
    <div className="footer-container">
      <div className="footer-detail">
        <div className="footer-detail-part">
          <strong>Hỗ trợ khách hàng</strong>
          <p />
          <p className="footer-text-regular">Thẻ ưu đãi</p>
          <p className="footer-text-regular">Trung tâm bảo hành</p>
          <p className="footer-text-regular">Thanh toán và giao hàng</p>
          <p className="footer-text-regular">Dịch vụ sửa chữa và bảo trì</p>
          <p className="footer-text-regular">Doanh nghiệp thân thiết</p>
        </div>
        <div className="footer-detail-part-large">
          <strong>Chính sách mua hàng vào bảo hành</strong>
          <p />
          <p className="footer-text-regular">Quy định chung</p>
          <p className="footer-text-regular">Chính sách bảo mật và thông tin</p>
          <p className="footer-text-regular">
            Chính sách vận chuyển và lắp đặt
          </p>
          <p className="footer-text-regular">Chính sách bảo hành</p>
          <p className="footer-text-regular">Chính sách đổi trả và hoàn tiền</p>
          <p className="footer-text-regular">
            Chính sách giá cả và hình thức thanh toán
          </p>
          <p className="footer-text-regular">Chính sách trả góp</p>
        </div>
        <div className="footer-detail-part">
          <strong>Thôn tin Ecommerce</strong>
          <p />
          <p className="footer-text-regular">Giới thiệu</p>
          <p className="footer-text-regular">Thông tin liên hệ</p>
          <p className="footer-text-regular">Hệ thống showroom</p>
          <p className="footer-text-regular">Hỏi đáp</p>
          <p className="footer-text-regular">Tin công nghệ</p>
          <p className="footer-text-regular">Tuyển dụng</p>
        </div>
        <div className="footer-detail-part">
          <strong>Email liên hệ</strong>
          <p />
          <p className="footer-text-regular">Hỗ trợ khách hàng</p>
          <a className="footer-text-regular">khaibui@gmail.com</a>
          <p />
          <p className="footer-text-regular">Liên hệ báo giá</p>
          <a className="footer-text-regular">khaibui@gmail.com</a>
          <p />
          <p className="footer-text-regular">Hợp tác phát triển</p>
          <a className="footer-text-regular">khaibui@gmail.com</a>
        </div>
        <div className="footer-detail-part">
          <strong>Phương thức thanh toán</strong>
          <p />
          <ButtonGroup vertical minimal alignText="left">
            <Button icon="credit-card" text="Credit card" />
            <Button icon="mobile-phone" text="Smart banking" />
            <Button icon="dollar" text="Tiền mặt" />
            <Button icon="time" text="Trả góp" />
          </ButtonGroup>
        </div>
      </div>

      <div className="footer-address">
        <div class="w-50 mr-5">
          <p className="footer-text-bold">
            CÔNG TY CỔ PHẦN THƯƠNG MẠI - DỊCH VỤ ECOMMERCE
          </p>
          <p className="footer-text-regular">
            1997 - 2020 Công Ty Cổ Phần Thương Mại - Dịch Vụ Ecommerce
          </p>
          <p className="footer-text-regular">
            Giấy chứng nhận đăng ký doanh nghiệp: 0304998358 do Sở KH-ĐT TP.HCM
            cấp lần đầu ngày 24 tháng 04 năm 2021
          </p>
        </div>
        <div class="w-50">
          <p className="footer-text-bold">Địa chỉ trụ sở chính:</p>
          <p className="footer-text-regular">
            Tầng 3, phòng 322 toàn nhà A6, Kí túc xá ĐHQG, Quận Thủ Đức, TP. Thủ
            Đức
          </p>
          <p className="footer-text-bold">Văn phòng điều hành miền Bắc:</p>
          <p className="footer-text-regular">
            Tầng 6, Số 1 Phố Thái Hà, Phường Trung Liệt, Quận Đống Đa, Hà Nội
          </p>
          <p className="footer-text-bold">Văn phòng điều hành miền Nam:</p>
          <p className="footer-text-regular">
            Tầng 2 tòa nhà C6, đường Lý Thường Kiệt, Quận 10, TP. Hồ Chí Minh
          </p>
        </div>
      </div>
    </div>
  );
}
