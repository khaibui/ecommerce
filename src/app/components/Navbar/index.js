import React from "react";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Button, Navbar, NavbarGroup } from "@blueprintjs/core";

import ConfirmDialog from "../../components/ConfirmDialog";

import * as FirebaseService from "../../firebase/service";
import * as Actions from "../../utils/store/action";

const NavbarComponent = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const userStatus = useSelector(({ User }) => User.User.userStatus);

  const onHomeClick = () => history.push("/");
  const onSignInClick = () =>
    userStatus ? history.push("/user/profile") : history.push("/sign-in");

  const onLogoutClick = () => {
    dispatch(
      Actions.openDialog(
        "Xác nhận đăng xuất",
        "Bạn có muốn đăng xuất khỏi tài khoản.",
        () => {
          FirebaseService.signOut();
          history.push("/");
        },
        "log-out"
      )
    );
  };

  return (
    <React.Fragment>
      <ConfirmDialog />
      <Navbar className="bp3-dark navbar-container" fixedToTop>
        <NavbarGroup align="right">
          <Button
            minimal
            icon="user"
            text={userStatus ? userStatus.email : "Đăng nhập"}
            onClick={onSignInClick}
          />
          <Button minimal icon="home" text="Trang chủ" onClick={onHomeClick} />
          <Button minimal icon="comment" text="Tư vấn mua hàng" />
          <Button minimal icon="headset" text="CSKH: 0898463002" />
          {userStatus && (
            <Button
              minimal
              icon="log-out"
              text="Đăng xuất"
              onClick={onLogoutClick}
            />
          )}
        </NavbarGroup>
      </Navbar>
    </React.Fragment>
  );
};

export default NavbarComponent;
