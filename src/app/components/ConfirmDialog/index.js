import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Classes, Dialog } from "@blueprintjs/core";

import * as Actions from "../../utils/store/action";

const ConfirmDialog = (props) => {
  const dispatch = useDispatch();

  const openDialog = useSelector(({ User }) => User.User.openDialog);

  const onCancelClick = () => dispatch(Actions.closeDialog());

  const onConfirm = () => {
    if (openDialog) {
      openDialog.onConfirm();
      dispatch(Actions.closeDialog());
    }
  };

  return (
    <Dialog
      isOpen={openDialog?.open}
      icon={openDialog?.icon || "confirm"}
      title={openDialog?.title || ""}
      onClose={onCancelClick}
    >
      <div className={Classes.DIALOG_BODY}>
        <div>{openDialog?.message || ""}</div>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <button className="user-cart-button-remove" onClick={onCancelClick}>
            Bỏ qua
          </button>
          <button className="user-profile-button" onClick={onConfirm}>
            Xác nhận
          </button>
        </div>
      </div>
    </Dialog>
  );
};

export default ConfirmDialog;
