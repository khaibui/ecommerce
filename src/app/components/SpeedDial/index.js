import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { SpeedDial, SpeedDialAction } from "@material-ui/lab";
import {
  ShoppingCart,
  AccountCircle,
  LocalOffer,
  TurnedIn,
} from "@material-ui/icons";

export default function SpeedDials(props) {
  const history = useHistory();

  const onSignInClick = () => history.push("/sign-in");
  const onCartClick = () => history.push("/user/cart");
  const onBillClick = () => history.push("/user/wait");

  const actions = [
    { icon: <ShoppingCart />, name: "Giỏ hàng", onClick: onCartClick },
    { icon: <AccountCircle />, name: "Đăng nhập", onClick: onSignInClick },
    { icon: <TurnedIn />, name: "Đơn hàng", onClick: onBillClick },
  ];

  return (
    <div className="speed-dial">
      <SpeedDial
        ariaLabel="SpeedDial example"
        hidden
        open={!props?.hidden}
        direction="up"
      >
        {actions.map((action) => (
          <SpeedDialAction
            key={action.name}
            icon={action.icon}
            tooltipTitle={action.name}
            onClick={action.onClick}
          />
        ))}
      </SpeedDial>
    </div>
  );
}
