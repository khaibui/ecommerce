import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

export default function SnackbarCustom(props) {
  const time = props?.time || 3000;
  const type = ["success", "error", "warning", "info"].includes(props?.type)
    ? props?.type
    : "success";
  const text = props?.text || "Vui lòng kiểm tra thông tin";

  return (
    <Snackbar
      variant="filled"
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      open={props.open}
      autoHideDuration={time}
      onClose={props.onClose}
    >
      <MuiAlert onClose={props.onClose} severity={type}>
        {text}
      </MuiAlert>
    </Snackbar>
  );
}
