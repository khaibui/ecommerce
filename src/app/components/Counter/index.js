import React, { useState } from "react";
import { Button } from "@blueprintjs/core";

import * as FirebaseService from "../../firebase/service";
import "../../../style/component/counter.scss";

const Counter = (props) => {
  const currentValue = props.counterValue;

  const onChangeCounter = async (counterParam) => {
    const cartUpdate = props.currentList.map((item) =>
      item.productCode === props.cartDetail.productCode
        ? { ...item, productTotal: counterParam }
        : { ...item }
    );
    await FirebaseService.updateOneDataFromStore(
      "users",
      props.userData.userId,
      {
        userCart: cartUpdate,
      }
    );
  };

  return (
    <div {...props} className="counter-container">
      <Button
        icon="minus"
        minimal
        onClick={() => onChangeCounter(currentValue - 1)}
        disabled={currentValue <= 1}
      />
      <p className="counter-edit-text">{currentValue}</p>
      <Button
        icon="plus"
        minimal
        onClick={() => onChangeCounter(currentValue + 1)}
        disabled={currentValue >= props.cartDetail.productRemaind}
      />
    </div>
  );
};

export default Counter;
