import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import withReducer from "../store/withReducer";
import reducer from "../utils/store/reducer";

import Firebase from "../firebase/firebase";

import * as Actions from "../utils/store/action";
import * as FirebaseService from "../firebase/service";

const Auth = (props) => {
  const auth = Firebase.auth;
  const dispatch = useDispatch();

  const userStatus = useSelector(({ User }) => User.User.userStatus);
  const userData = useSelector(({ User }) => User.User.user);

  useEffect(async () => {
    await FirebaseService.getALlDataFromStore("products").then(
      async (snapshot) => {
        const dataSnapshot = snapshot.docs.map((item) => ({ ...item.data() }));
        dispatch(
          Actions.setProduct(
            dataSnapshot.filter((item) => item.productActive === "active")
          )
        );
      }
    );
  }, []);

  useEffect(() => {
    if (userStatus && userData) {
      const firestore = Firebase.firestore
        .collection("users")
        .doc(userData.userId);
      firestore.onSnapshot(
        (snapshot) => dispatch(Actions.setUser(snapshot.data())),
        (err) => console.log(err)
      );
    }
  }, [userStatus]);

  auth.onAuthStateChanged(async (user) => {
    if (user) {
      dispatch(Actions.setUserStatus(user));
      await FirebaseService.getOneDataFromStore("users", user.uid).then(
        (snapshot) => dispatch(Actions.setUser(snapshot.data()))
      );
    } else {
      console.log("Không tìm thấy thông tin user");
      dispatch(Actions.setUserStatus(null));
      dispatch(Actions.setUser(null));
    }
  });

  return <React.Fragment>{props.children}</React.Fragment>;
};

export default withReducer("User", reducer)(Auth);
