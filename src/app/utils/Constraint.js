export const ADMIN_ACCOUNT = [
  "admin@gmail.com",
  "admin@admin.com",
  "admim@ecommerce.com",
  "admin@manage.com",
  "admin@contact.com",
];

export const categories = [
  {
    cateCode: "apple",
    cateName: "Apple",
    cateDetail: {
      rightImage: "/images/24.png",
      description: "",
    },
  },
  {
    cateCode: "asus",
    cateName: "ASUS",
    cateDetail: {
      rightImage: "/images/24.png",
      description: "",
    },
  },
];

export const highlightData = [
  {
    title: "MSI",
    cateCode: "msi",
    script: "Dành riêng cho Gamers và Creators",
    image: "/images/14.png",
  },
  {
    title: "HP",
    cateCode: "hp",
    script: "Lễ hội máy tính HP - Ưu đãi cực phê",
    image: "/images/26.png",
  },
  {
    title: "LG",
    cateCode: "lg",
    script: "Tháng LG quà mê ly",
    image: "/images/12.png",
  },
  {
    title: "Lenovo",
    cateCode: "lenovo",
    script: "Laptop chơi game thực thụ",
    image: "/images/11.png",
  },
];

export const filterEngine = [
  {
    code: "byCate",
    text: "Thương hiệu",
    detail: [
      { code: "apple", name: "Apple" },
      { code: "dell", name: "Dell" },
      { code: "asus", name: "ASUS" },
      { code: "hp", name: "HP" },
      { code: "accer", name: "ACER" },
      { code: "lenovo", name: "Lenovo" },
      { code: "msi", name: "MSI" },
      { code: "lg", name: "LG" },
      { code: "huawei", name: "Huawei" },
    ],
  },
  {
    code: "byCpu",
    text: "Series CPU",
    detail: [
      { code: "i5", name: "Core i5" },
      { code: "i7", name: "Core i7" },
      { code: "i3", name: "Core i3" },
      { code: "ryzen5", name: "Ryzen 5" },
      { code: "ryzen7", name: "Ryzen 7" },
      { code: "ryzen3", name: "Ryzen 3" },
      { code: "pentinum", name: "Pentinum" },
    ],
  },
  {
    code: "bySize",
    text: "Kích thước màn hình",
    detail: [
      { code: "16.5", name: `16.5"` },
      { code: "14", name: `14"` },
      { code: "13", name: `13"` },
      { code: "13.3", name: `13.3"` },
      { code: "13.4", name: `13.4"` },
      { code: "13.5", name: `13.5"` },
      { code: "17.3", name: `17.3"` },
      { code: "15", name: `15"` },
    ],
  },
  {
    code: "byQud",
    text: "Chuẩn phân giải",
    detail: [
      { code: "full Hd", name: "Full HD" },
      { code: "hd", name: "HD" },
      { code: "wq hd", name: "WQHD" },
      { code: "full Hd Plus", name: "Full HD+" },
    ],
  },
  {
    code: "byRam",
    text: "Dung lượng RAM",
    detail: [
      { code: "8", name: "8 GB" },
      { code: "4", name: "4 GB" },
      { code: "16", name: "16 GB" },
      { code: "32", name: "32 GB" },
    ],
  },
];

export const sortEngine = [
  { code: "discount", text: "Khuyến mãi tốt nhất" },
  { code: "increase", text: "Giá tăng dần" },
  { code: "decrease", text: "Giá giảm dần" },
];

// export const products = [
//   {
//     productCode: "200201374",
//     cateCode: "asus",
//     productPrice: 24490000,
//     productDiscount: 10,
//     productFreeShip: true,
//     productTotal: 100,
//     productRemaind: 12,
//     productTitle: "Laptop ASUS ZenBook 14",
//     productTitleScript:
//       'UX434FAC-A6116T (14" FHD/i5-10210U/8GB/512GB SSD/Intel UHD/Win10/1.3kg)',
//     productImage: "",
//     productActive: true,
//     productDetail: {
//       guarantee: "24 tháng",
//       color: "Silver",
//       series: "Zenbook",
//       partNumber: "A6116T",
//       cpuGen: "Core i5, Intel Core thế hệ thứ 10",
//       cpu: "Intel Core i5-10210U ( 1.6 GHz - 4.2 GHz / 6MB / 4 nhân, 8 luồng)",
//       graphicChip: "Intel UHD Graphics",
//       ram: "8GB Onboard LPDDR3 2133MHz không thể nâng cấp)",
//       display:
//         '14" ( 1920 x 1080 ) Full HD không cảm ứng , Màn hình chống lóa , HD webcam',
//       store: "512GB SSD M.2 NVMe",
//       graphicGate: "1 x HDMI",
//       gate:
//         "1 x USB Type C , 1 x USB 3.1 , 1 x USB 2.0 , 1 x micro SD card slot",
//       wireless: "WiFi 802.11ax (Wifi 6) , Bluetooth 5.0",
//       board: "Thường , không phím số , LED",
//       os: "Windows 10",
//       size: "31.9 x 19.9 x 1.69 cm",
//       pin: "3 cell 50 Wh , Pin liền",
//       weight: "1.3 kg",
//       security: "Khuôn mặt",
//       led: "Không đèn",
//       service: "Màn hình phụ tích hợp trong Touchpad (ScreenPad)",
//       moreDevice: "Sleeve, USB3.0 to RJ45 Cable",
//     },
//   },
// ];

// export const importProduct = {
//   productCode: "201000397",
//   cateCode: "asus",
//   productPrice: 24990000,
//   productDiscount: 6,
//   productFreeShip: true,
//   productTotal: 100,
//   productRemaind: 83,
//   productTitle: "Laptop ASUS ROG Strix G",
//   productTitleScript:
//     'G531GT HN554T ( 15.6" Full HD/ 144Hz/Intel Core i7 9750H/8GB/512GB SSD/NVIDIA GeForce GTX 1650/Win 10 Home SL)',
//   productImage: "",
//   productActive: true,
//   productDetail: {
//     guarantee: "24 tháng",
//     color: "Đen",
//     series: "ROG",
//     partNumber: "HN554T",
//     cpuGen: "Core i7 , Intel Core thế hệ thứ 9",
//     cpu: "Intel Core i7 9750H ( 2.6 GHz - 4.5 GHz / 12MB / 6 nhân, 12 luồng )",
//     graphicChip: "Intel UHD Graphics",
//     ram: "1 x 8GB DDR4 2666MHz ( 2 Khe cắm / Hỗ trợ tối đa 32GB )",
//     display:
//       '14" ( 1920 x 1080 ) Full HD không cảm ứng , Màn hình chống lóa , HD webcam',
//     store: "512GB SSD M.2 NVMe",
//     graphicGate: "1 x HDMI",
//     gate: "1 x USB Type C , 1 x USB 3.1 , 1 x USB 2.0 , 1 x micro SD card slot",
//     wireless: "WiFi 802.11ax (Wifi 6) , Bluetooth 5.0",
//     board: "Thường , không phím số , LED",
//     os: "Windows 10",
//     size: "31.9 x 19.9 x 1.69 cm",
//     pin: "3 cell 50 Wh , Pin liền",
//     weight: "1.3 kg",
//     security: "Khuôn mặt",
//     led: "Không đèn",
//     service: "Màn hình phụ tích hợp trong Touchpad (ScreenPad)",
//     moreDevice: "Sleeve, USB3.0 to RJ45 Cable",
//   },
// };

// export const users = [
//   {
//     userId: "20200504",
//     userName: "khaibui",
//     userPassword: "khaibui",
//     userProfile: {
//       userFullname: "Bùi Quốc Khải",
//       userEmail: "khaibui@gmail.com",
//       userPhone: "0898463002",
//       userAddress:
//         "Đại học Bách Khoa, ĐHQG - TPHCM, phường Linh Trung, quận Thủ Đức",
//     },
//     userSale: [],
//     userBill: [
//       {
//         billCode: "05052021",
//         billCreatedAt: "2021/05/05",
//         billTotal: 451250000,
//         billStatus: "success",
//         billProduct: [
//           {
//             productCode: "200201374",
//             productTitle: "Laptop ASUS ZenBook 14",
//             productTotal: 2,
//             productPrice: 24490000,
//             productFinalPrice: 24110000,
//             productDiscount: 10,
//             productFreeship: true,
//           },
//           {
//             productCode: "200201374",
//             productTitle: "Laptop ASUS ZenBook 14",
//             productTotal: 2,
//             productPrice: 24490000,
//             productFinalPrice: 24110000,
//             productDiscount: 10,
//             productFreeship: true,
//           },
//         ],
//       },
//       {
//         billCode: "05052021",
//         billCreatedAt: "2021/05/05",
//         billTotal: 451250000,
//         billStatus: "success",
//         billProduct: [
//           {
//             productCode: "200201374",
//             productTitle: "Laptop ASUS ZenBook 14",
//             productTotal: 2,
//             productPrice: 24490000,
//             productFinalPrice: 24110000,
//             productDiscount: 10,
//             productFreeship: true,
//           },
//           {
//             productCode: "200201374",
//             productTitle: "Laptop ASUS ZenBook 14",
//             productTotal: 2,
//             productPrice: 24490000,
//             productFinalPrice: 24110000,
//             productDiscount: 10,
//             productFreeship: true,
//           },
//         ],
//       },
//     ],
//     userCart: [
//       {
//         productCode: "200201374",
//         productTotal: 2,
//       },
//     ],
//   },
// ];
