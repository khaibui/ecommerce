import { makeStyles } from "@material-ui/core/styles";

export const convertNameToText = (varParam) => {
  const firstLetterUpper = varParam[0].toUpperCase();
  const varNameUpperFirst = firstLetterUpper + varParam.slice(1);
  return varNameUpperFirst.match(/[A-Z][a-z]+|[0-9]+/g).join(" ");
};

export const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 10000000,
    color: "#fff",
  },
}));
