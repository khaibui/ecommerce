import axios from "axios";

export const REACT_APP_END_POINT = "http://localhost:8080";

export const API_EP = axios.create({
  baseURL: REACT_APP_END_POINT,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  timeout: 30000,
});
