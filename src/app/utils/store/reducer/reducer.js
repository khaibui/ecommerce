import * as Actions from "../action";

const initialState = {
  user: null,
  userStatus: null,
  products: [],
  allUsers: [],
  openDialog: {},
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.USER_DETAIL:
      return {
        ...state,
        user: action.user,
      };

    case Actions.USER_STATUS:
      return {
        ...state,
        userStatus: action.status,
      };

    case Actions.PRODUCTS:
      return {
        ...state,
        products: action.products,
      };

    case Actions.ALL_ACCOUNT:
      return {
        ...state,
        allUsers: action.allUsers,
      };

    case Actions.TOGGLE_DIALOG:
      return {
        ...state,
        openDialog: action.params,
      };

    default:
      return state;
  }
};

export default userReducer;
