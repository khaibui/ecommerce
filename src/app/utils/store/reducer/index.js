import { combineReducers } from "redux";

import User from "./reducer";

const reducer = combineReducers({
  User,
});

export default reducer;
