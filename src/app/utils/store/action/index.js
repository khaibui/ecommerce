export const USER_DETAIL = "USER_DETAIL";
export const USER_STATUS = "USER_STATUS";
export const PRODUCTS = "PRODUCTS";
export const ALL_ACCOUNT = "ALL_ACCOUNT";
export const OPEN_CONFIRM = "OPEN_CONFIRM";
export const TOGGLE_DIALOG = "TOGGLE_DIALOG";

export const setUser = (user) => (dispatch) =>
  dispatch({ type: USER_DETAIL, user });

export const setUserStatus = (status) => (dispatch) =>
  dispatch({ type: USER_STATUS, status });

export const setProduct = (products) => (dispatch) =>
  dispatch({ type: PRODUCTS, products });

export const setAllUserAccount = (allUsers) => (dispatch) =>
  dispatch({ type: ALL_ACCOUNT, allUsers });

export const openDialog = (title, message, onConfirm, icon) => (dispatch) =>
  dispatch({
    type: TOGGLE_DIALOG,
    params: { open: true, title, message, onConfirm, icon },
  });

export const closeDialog = () => (dispatch) =>
  dispatch({
    type: TOGGLE_DIALOG,
    params: { open: false },
  });
