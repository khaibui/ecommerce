import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Button, InputGroup } from "@blueprintjs/core";
import { Backdrop, CircularProgress } from "@material-ui/core";

import Navbar from "../../components/Navbar";
import Footer from "../../components/Footer";
import Snackbar from "../../components/Snackbar";

import * as FirebaseService from "../../firebase/service";
import * as Functions from "../../utils/CommonFunction";

const SignInPage = (props) => {
  const history = useHistory();
  const classes = Functions.useStyles();

  const [backDropOpen, setBackDropOpen] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [warningShow, setWarningShow] = useState(false);
  const [emailText, setEmailText] = useState("");
  const [passwordText, setPasswordText] = useState("");
  const [snackbarDetail, setSnackbarDetail] = useState({
    open: false,
    type: "success",
    text: "",
  });

  const onUpdateSnackbar = (openParam, typeParam, textParam) =>
    setSnackbarDetail((snackbarDetail) => ({
      ...snackbarDetail,
      open: openParam,
      type: typeParam,
      text: textParam,
    }));

  const validateSignInForm = () => {
    const fillFullForm = emailText.length && passwordText.length;
    if (!fillFullForm)
      onUpdateSnackbar(
        true,
        "warning",
        "Bạn vui lòng kiểm tra đầy đủ thông tin"
      );
    setWarningShow(!fillFullForm);
    return fillFullForm;
  };

  const onSignInClick = async () => {
    if (validateSignInForm()) {
      setBackDropOpen(true);
      await FirebaseService.signInByEmailPassword(emailText, passwordText)
        .then((userCredential) => {
          if (userCredential) history.push("/");
        })
        .catch((err) => onUpdateSnackbar(true, "warning", err.message));

      setBackDropOpen(false);
    }
  };

  const onShowPasswordClick = () => setShowPassword(!showPassword);
  const onSnackbarClose = () => onUpdateSnackbar(false, "", "");
  const onSignUpClick = () => history.push("/sign-up");
  const onEmailChange = (e) => setEmailText(e.target.value);
  const onPasswordChange = (e) => setPasswordText(e.target.value);

  return (
    <div>
      <Backdrop className={classes.backdrop} open={backDropOpen}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Navbar />
      <img className="image" src={process.env.PUBLIC_URL + "/images/10.png"} />
      <div className="sign-in-container">
        <div className="sign-in-title">Chào mừng bạn đến với Ecommerce</div>
        <InputGroup
          large
          className="sign-in-input"
          placeholder="Email"
          onChange={onEmailChange}
          intent={warningShow && !emailText.length ? "danger" : "none"}
        />
        <InputGroup
          large
          type={showPassword ? "text" : "password"}
          placeholder="Mật khẩu"
          className="sign-in-input"
          onChange={onPasswordChange}
          intent={warningShow && !passwordText.length ? "danger" : "none"}
          rightElement={
            <Button
              minimal
              icon={showPassword ? "eye-open" : "eye-off"}
              onClick={onShowPasswordClick}
            />
          }
        />
        <button className="sign-in-button" onClick={onSignInClick}>
          Đăng nhập
        </button>
        <button className="sign-up-button" onClick={onSignUpClick}>
          Đăng ký
        </button>
      </div>
      <Footer />
      <Snackbar
        open={snackbarDetail.open}
        type={snackbarDetail.type}
        text={snackbarDetail.text}
        onClose={onSnackbarClose}
      />
    </div>
  );
};

export default SignInPage;
