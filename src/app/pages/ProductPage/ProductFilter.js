import React, { useState, useEffect } from "react";

import { filterEngine } from "../../utils/Constraint";

const ProductFilter = (props) => {
  const onClickSearch = (engineCode, itemCode) => {
    const oldEngineData = props.filter[engineCode];
    const isExist = oldEngineData.includes(itemCode);
    const newEngineData = isExist
      ? oldEngineData.filter((item) => item !== itemCode)
      : [...oldEngineData, itemCode];

    const updateFilter = {
      ...props.filter,
      [engineCode]: newEngineData,
    };
    props.onFilterChange(updateFilter);
  };

  return (
    <div {...props} className="product-filter-container">
      {filterEngine.map((engine) => (
        <div key={engine.code} className="product-cate-container">
          <p className="product-cate-name">{engine.text}</p>
          <div className="product-cate-container-item">
            {engine.detail.map((item) => (
              <div
                key={item.code}
                id={item.code}
                className={
                  props.filter[engine.code].includes(item.code)
                    ? "product-cate-item-select"
                    : "product-cate-item"
                }
                onClick={() => onClickSearch(engine.code, item.code)}
              >
                {item.name}
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
};

export default ProductFilter;
