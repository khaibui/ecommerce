import React from "react";
import AwesomeSlider from "react-awesome-slider";
import withAutoplay from "react-awesome-slider/dist/autoplay";

import "react-awesome-slider/dist/styles.css";

const AutoplaySlider = withAutoplay(AwesomeSlider);

const ProductHeadSlideshow = (props) => {
  return (
    <AutoplaySlider
      className="product-slideshow"
      play={true}
      bullets={false}
      cancelOnInteraction={false}
      interval={4000}
    >
      {[...Array(7).keys()].map((item) => (
        <div
          style={{ borderRadius: "10px" }}
          data-src={process.env.PUBLIC_URL + `/product-slideshow/${item}.png`}
        />
      ))}
    </AutoplaySlider>
  );
};

export default ProductHeadSlideshow;
