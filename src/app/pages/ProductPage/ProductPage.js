import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { Button } from "@blueprintjs/core";
import { Tooltip2 } from "@blueprintjs/popover2";

import Navbar from "../../components/Navbar";
import MainHeader from "../../components/MainHeader";
import Footer from "../../components/Footer";
import ProductList from "../../components/ProductList";

import ProductHeadSlideshow from "./ProductHeadSlideshow";
import ProductFilter from "./ProductFilter";
import ProductSort from "./ProductSort";

const MainPage = (props) => {
  const params = useParams();

  const [moreFactor, setMoreFactor] = useState(1);
  const [filterEngineData, setFilterEngineData] = useState({
    byCate: [params.category],
    byRam: [],
    byQud: [],
    bySize: [],
    byCpu: [],
  });
  const [sorterEngineData, setSorterEngineData] = useState({
    sorter: "",
    priceScale: [0, 100],
  });

  const onMoreClick = () => setMoreFactor((preState) => preState + 1);
  const onFilterChange = (filterParam) => setFilterEngineData(filterParam);
  const onSorterChange = (sorterParam) => setSorterEngineData(sorterParam);

  return (
    <div {...props}>
      <Navbar />
      <img className="image" src={process.env.PUBLIC_URL + "/images/10.png"} />
      <div className="container">
        <MainHeader />
        <div className="product-sub-container">
          <ProductHeadSlideshow />
          <div className="group-container">
            <div className="title">
              <div className="title-mark" />
              <div className="filter-title">Bộ lọc</div>
              <div className="filter-clear">
                <Tooltip2 placement="right" content="Bỏ chọn tất cả">
                  <Button minimal icon="filter-remove" />
                </Tooltip2>
              </div>
            </div>
            <ProductFilter
              onFilterChange={onFilterChange}
              filter={filterEngineData || {}}
            />
          </div>

          <div className="group-container">
            <ProductSort
              onSorterChange={onSorterChange}
              sorter={sorterEngineData || {}}
            />
          </div>

          <div className="group-container">
            <ProductList
              filter={filterEngineData || {}}
              sorter={sorterEngineData || {}}
              moreFactor={moreFactor}
            />
            <a className="product-see-all" onClick={onMoreClick}>
              More
            </a>
          </div>
        </div>

        <Footer />
      </div>
    </div>
  );
};

export default MainPage;
