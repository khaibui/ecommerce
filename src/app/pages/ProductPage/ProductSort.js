import React from "react";
import { RangeSlider } from "@blueprintjs/core";

import { sortEngine } from "../../utils/Constraint";

export default function ProductSort(props) {
  const onClickSearch = (engineCode) => {
    engineCode === props.sorter.sorter
      ? props.onSorterChange({ ...props.sorter, sorter: "" })
      : props.onSorterChange({ ...props.sorter, sorter: engineCode });
  };

  const onPriceSliderChange = (range) =>
    props.onSorterChange({ ...props.sorter, priceScale: range });

  return (
    <div className="product-sort-container">
      <div className="product-cate-container">
        <p className="product-cate-name">Sắp xếp theo</p>
        <div className="product-cate-container-item">
          {sortEngine.map((item) => (
            <div
              key={item.code}
              id={item.code}
              className={
                props.sorter.sorter === item.code
                  ? "product-sort-item-select"
                  : "product-sort-item"
              }
              onClick={() => onClickSearch(item.code)}
            >
              {item.text}
            </div>
          ))}
        </div>
      </div>

      <div className="product-cate-container">
        <p className="product-cate-name">Khoảng giá</p>
        <p className="product-slider-container">
          <RangeSlider
            min={0}
            max={100}
            stepSize={1}
            labelStepSize={5}
            vertical={false}
            value={props.sorter.priceScale}
            onChange={onPriceSliderChange}
          />
        </p>
      </div>
    </div>
  );
}
