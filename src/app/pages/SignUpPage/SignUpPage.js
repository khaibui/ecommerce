import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Button, InputGroup, Checkbox } from "@blueprintjs/core";

import Snackbar from "../../components/Snackbar";
import Navbar from "../../components/Navbar";
import Footer from "../../components/Footer";

import * as FirebaseService from "../../firebase/service";

const SignInPage = (props) => {
  const history = useHistory();
  const [showPassword, setShowPassword] = useState(false);
  const [allowPolicy, setAllowPolicy] = useState(false);
  const [warningShow, setWarningShow] = useState(false);
  const [emailText, setEmailText] = useState("");
  const [passwordText, setPasswordText] = useState("");
  const [fullNameText, setFullNameText] = useState("");
  const [phoneText, setPhoneText] = useState("");
  const [addressText, setAddressText] = useState("");
  const [snackbarDetail, setSnackbarDetail] = useState({
    open: false,
    type: "success",
    text: "",
  });

  const onUpdateSnackbar = (openParam, typeParam, textParam) =>
    setSnackbarDetail((snackbarDetail) => ({
      ...snackbarDetail,
      open: openParam,
      type: typeParam,
      text: textParam,
    }));

  const validateSignUpForm = () => {
    const fillFullForm =
      emailText.length &&
      passwordText.length &&
      fullNameText.length &&
      phoneText.length &&
      addressText.length &&
      allowPolicy;
    if (!fillFullForm)
      onUpdateSnackbar(
        true,
        "warning",
        "Bạn vui lòng kiểm tra đầy đủ thông tin"
      );
    setWarningShow(!fillFullForm);
    return fillFullForm;
  };

  const onSignUpClick = () => {
    if (validateSignUpForm()) {
      FirebaseService.createUserByEmailPassword(emailText, passwordText)
        .then(async (userCredential) => {
          if (userCredential) {
            const uid = userCredential.user.uid;
            const email = userCredential.user.email;
            const userInitial = {
              status: "Active",
              userId: uid,
              userEmail: email,
              userProfile: {
                userFullName: fullNameText,
                userEmail: email,
                userPhone: phoneText,
                userAddress: addressText,
              },
            };
            await FirebaseService.createUserToStore("users", uid, userInitial);
            history.push("/");
          }
        })
        .catch((err) => onUpdateSnackbar(true, "warning", err.message));
    }
  };

  const onShowPasswordClick = () => setShowPassword(!showPassword);
  const onPolicyClick = () => setAllowPolicy(!allowPolicy);
  const onSnackbarClose = () => onUpdateSnackbar(false, "", "");
  const onSignInClick = () => history.push("sign-in");
  const onEmailChange = (e) => setEmailText(e.target.value);
  const onPasswordChange = (e) => setPasswordText(e.target.value);
  const onFullNameChange = (e) => setFullNameText(e.target.value);
  const onPhoneChange = (e) => setPhoneText(e.target.value);
  const onAddressChange = (e) => setAddressText(e.target.value);

  return (
    <React.Fragment>
      <Navbar />
      <img className="image" src={process.env.PUBLIC_URL + "/images/10.png"} />
      <div className="sign-in-container">
        <div className="sign-in-title">Đăng ký tài khoản Ecommerce</div>
        <InputGroup
          large
          className="sign-in-input"
          placeholder="Email"
          onChange={onEmailChange}
          intent={warningShow && !emailText.length ? "danger" : "none"}
        />
        <InputGroup
          large
          type={showPassword ? "text" : "password"}
          placeholder="Mật khẩu"
          className="sign-in-input"
          onChange={onPasswordChange}
          intent={warningShow && !passwordText.length ? "danger" : "none"}
          rightElement={
            <Button
              minimal
              icon={showPassword ? "eye-open" : "eye-off"}
              onClick={onShowPasswordClick}
            />
          }
        />
        <InputGroup
          large
          className="sign-in-input"
          placeholder="Tên đầy đủ"
          onChange={onFullNameChange}
          intent={warningShow && !fullNameText.length ? "danger" : "none"}
        />
        <InputGroup
          large
          className="sign-in-input"
          placeholder="Số điện thoại"
          onChange={onPhoneChange}
          intent={warningShow && !phoneText.length ? "danger" : "none"}
        />
        <InputGroup
          large
          className="sign-in-input"
          placeholder="Địa chỉ giao hàng"
          onChange={onAddressChange}
          intent={warningShow && !addressText.length ? "danger" : "none"}
        />
        <div className="sign-up-polocy-container">
          <Checkbox
            checked={allowPolicy}
            label="Đồng ý với các"
            onClick={onPolicyClick}
          />
          <p className="sign-up-polocy">điều khoản sử dụng</p>
        </div>
        <button className="sign-in-button" onClick={onSignUpClick}>
          Đăng ký
        </button>
        <button className="sign-up-button" onClick={onSignInClick}>
          Đăng nhập
        </button>
      </div>
      <Footer />
      <Snackbar
        open={snackbarDetail.open}
        type={snackbarDetail.type}
        text={snackbarDetail.text}
        onClose={onSnackbarClose}
      />
    </React.Fragment>
  );
};

export default SignInPage;
