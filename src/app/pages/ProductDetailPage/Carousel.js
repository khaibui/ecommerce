import React from "react";
import AwesomeSlider from "react-awesome-slider";
import withAutoplay from "react-awesome-slider/dist/autoplay";

import "react-awesome-slider/dist/styles.css";
import "../../../style/component/carousel.scss";

const AutoplaySlider = withAutoplay(AwesomeSlider);

const Carousel = (props) => {
  return (
    <div {...props} className="carousel-container">
      <AutoplaySlider
        play={true}
        bullets={false}
        cancelOnInteraction={false}
        interval={3000}
      >
        {props.product.image?.map((item) => (
          <div data-src={item} />
        ))}
      </AutoplaySlider>
    </div>
  );
};

export default Carousel;
