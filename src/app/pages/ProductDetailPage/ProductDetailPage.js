import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { Breadcrumbs, Button } from "@blueprintjs/core";

import Navbar from "../../components/Navbar";
import MainHeader from "../../components/MainHeader";
import Footer from "../../components/Footer";
import ProductList from "../../components/ProductList";

import Carousel from "./Carousel";
import Dialog from "./Dialog";

import * as Constraints from "../../utils/Constraint";
import * as CommonFunctions from "../../utils/CommonFunction";
import * as FirebaseService from "../../firebase/service";

const CustomBreadcrumbs = (props) => {
  const itemArray = [
    { href: "/", text: "Home" },
    { href: "/product/asus", text: "ASUS" },
    { text: "Laptop ASUS ZenBook 14", current: true },
  ];

  return <Breadcrumbs {...props} items={itemArray} overflowListProps={false} />;
};

const BuyButtonGroup = (props) => {
  const history = useHistory();

  const userStatus = useSelector(({ User }) => User.User.userStatus);

  const onAddItemClick = async () => {
    if (userStatus) {
      // update user cart
      const productCode = props.product.productCode;
      const userCateData = Array.isArray(props.userData.userCart)
        ? props.userData.userCart.map((item) => ({ ...item }))
        : [];
      const productExisted = userCateData.find(
        (item) => item.productCode === productCode
      );
      const cartUpdate = productExisted
        ? userCateData.map((item) => {
            if (item.productCode === productCode) {
              const increaseTotal = item.productTotal + 1;
              return { ...item, productTotal: increaseTotal };
            }
            return { ...item };
          })
        : [
            ...userCateData,
            {
              productCode: productCode,
              productTotal: 1,
            },
          ];
      await FirebaseService.updateOneDataFromStore(
        "users",
        props.userData.userId,
        {
          userCart: cartUpdate,
        }
      );
      history.push("/user/cart");
    } else history.push("/sign-in");
  };

  return (
    <div className="detail-button-group-container">
      <button className="detail-button-buy" onClick={onAddItemClick}>
        MUA NGAY
      </button>
      <button className="detail-button-add" onClick={onAddItemClick}>
        THÊM HÀNG VÀO GIỎ
      </button>
    </div>
  );
};

const DetailInformation = (props) => {
  const titleFull = `${props.product?.productTitle} ${props.product?.productTitleScript}`;
  const originPrice = props.product?.productPrice;
  const hasDiscount = props.product?.productDiscount;
  const finalPrice =
    hasDiscount > 0 ? originPrice * (1 - hasDiscount / 100) : originPrice;
  return (
    <div {...props} className="detail-info-right">
      <p className="detail-info-title">{titleFull}</p>
      <div className="detail-info-script">
        <p>Thương hiệu</p>
        <a className="link" href={`/product/${props.product?.cateCode}`}>
          {props.cateData?.cateName}
        </a>
        <p>{`| SKU: ${props.product?.productCode}`}</p>
      </div>
      <div className="detail-info-price">
        <p className="detail-info-price-remaind">{`Còn lại ${props.product?.productRemaind} sản phẩm`}</p>
        <div className="detail-info-price-container">
          <p className="detail-info-price-final">{`${finalPrice?.toLocaleString()}đ`}</p>
          <p className="detail-info-price-origin">{`${originPrice?.toLocaleString()}đ`}</p>
        </div>
      </div>
      <BuyButtonGroup product={props.product} userData={props.userData} />
    </div>
  );
};

const PolicyArea = (props) => {
  const [isOpenDialog, setIsOpenDialog] = useState(false);

  const techInfoTableObj = props.product.productDetail || {};
  const techInfoTableData = Object.entries(techInfoTableObj).slice(0, 7);

  const onTableClick = () => setIsOpenDialog(true);
  const onTableCloseClick = () => setIsOpenDialog(false);

  return (
    <div {...props} className="detail-group-container">
      <div className="group-container-left">
        <div className="detail-group-freeship">
          <img
            className="product-list-item-freeship-icon"
            src={process.env.PUBLIC_URL + "/assets/free-ship.png"}
          />
          <p className="detail-group-left-freeship">
            Sản phẩm được miễn phí giao hàng
          </p>
        </div>
        <div className="detail-group-policy">
          <p className="detail-group-policy-title">Chính sách bán hàng</p>
          <Button
            minimal
            alignText="left"
            icon="tick-circle"
            text="Cam kết hàng chính hãng 100%"
          />
          <Button
            minimal
            alignText="left"
            icon="taxi"
            text="Miễn phí giao hàng từ 800K"
          />
          <Button
            minimal
            alignText="left"
            icon="automatic-updates"
            text="Đổi trả miễn phí trong 10 ngày"
          />
        </div>
        <div className="detail-group-policy">
          <p className="detail-group-policy-title">Dịch vụ khác</p>
          <Button
            minimal
            alignText="left"
            icon="wrench"
            text="Sửa chữa đồng giá 150.000đ"
          />
          <Button
            minimal
            alignText="left"
            icon="clean"
            text="Vệ sinh máy tính, laptop"
          />
          <Button
            minimal
            alignText="left"
            icon="home"
            text="Bảo hành tại nhà"
          />
        </div>
      </div>
      <div className="group-container-right" onClick={onTableClick}>
        <table class="bp3-html-table bp3-html-table-striped group-container-table">
          <thead>
            <tr>
              <th>Đặc điểm</th>
              <th>Thông số</th>
            </tr>
          </thead>
          <tbody>
            {techInfoTableData.map((row) => (
              <tr>
                <td>{CommonFunctions.convertNameToText(row[0])}</td>
                <td>{row[1]}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <Dialog
        product={props.product}
        isOpen={isOpenDialog}
        onClose={onTableCloseClick}
      />
    </div>
  );
};

const ProductDetail = (props) => {
  const params = useParams();

  const productData = useSelector(({ User }) => User.User.products);
  const userData = useSelector(({ User }) => User.User.user);

  const [currentProductData, setCurrentProductData] = useState({});
  const [currentCateData, setCurrentCateData] = useState({});

  useEffect(() => {
    if (productData && productData.length) {
      const productItemData = productData.find(
        (item) => item.productCode === params.product
      );
      const cateParentData = Constraints.categories.find(
        (item) => item.cateCode === productItemData?.cateCode
      );
      setCurrentProductData(productItemData);
      setCurrentCateData(cateParentData);
    }
  }, [productData]);

  return (
    <div {...props}>
      <Navbar />
      <img className="image" src={process.env.PUBLIC_URL + "/images/10.png"} />
      <div className="container">
        <MainHeader />
        <div className="product-sub-container">
          <CustomBreadcrumbs />
          <div className="group-container">
            <div className="detail-info-container">
              <Carousel product={currentProductData} />
              <DetailInformation
                cateData={currentCateData}
                product={currentProductData}
                userData={userData}
              />
            </div>
          </div>
          <PolicyArea product={currentProductData} />
          {/* <div className="group-container">
            <a href="/product/asus" className="product-see-all">
              Xem tất cả
            </a>
            <ProductList />
          </div> */}
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default ProductDetail;
