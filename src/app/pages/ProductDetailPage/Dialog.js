import React from "react";
import { Classes, Dialog } from "@blueprintjs/core";

import * as CommonFunctions from "../../utils/CommonFunction";

const CustomDialog = (props) => {
  const techInfoTableObj = props.product.productDetail || {};
  const techInfoTableData = Object.entries(techInfoTableObj);
  return (
    <Dialog {...props}>
      <div className={Classes.DIALOG_BODY}>
        <table class="bp3-html-table bp3-html-table-striped group-container-table">
          <thead>
            <tr>
              <th>Đặc điểm</th>
              <th>Thông số</th>
            </tr>
          </thead>
          <tbody>
            {techInfoTableData.map((row) => (
              <tr>
                <td>{CommonFunctions.convertNameToText(row[0])}</td>
                <td>{row[1]}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Dialog>
  );
};

export default CustomDialog;
