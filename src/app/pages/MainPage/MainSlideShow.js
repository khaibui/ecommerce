import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import AwesomeSlider from "react-awesome-slider";
import withAutoplay from "react-awesome-slider/dist/autoplay";
import { Menu } from "@blueprintjs/core";
import {
  AppleIcon,
  DellIcon,
  HpIcon,
  AcerIcon,
  AsusIcon,
  LenovoIcon,
  LgIcon,
  MsiIcon,
  HuaweiIcon,
} from "../../components/ColorIcons";
import MenuItem from "../../components/MenuItem";

import { categories } from "../../utils/Constraint";
import "react-awesome-slider/dist/styles.css";

const AutoplaySlider = withAutoplay(AwesomeSlider);

const MainSlideshow = (props) => {
  const history = useHistory();

  const [openCateDetail, setOpenCateDetail] = useState(false);
  const [mouseInCate, setMouseInCate] = useState(false);
  const [mouseInCateDetail, setMouseInCateDetail] = useState(false);
  const [cateCode, setCateCode] = useState("");

  const onMouseEnterCate = (cateCodeParam) => {
    setCateCode(cateCodeParam);
    setMouseInCate(true);
  };

  const onMouseLeaveCate = () => {
    setCateCode("");
    setMouseInCate(false);
  };

  const onMouseEnterCateDetail = () => setMouseInCateDetail(true);
  const onMouseLeaveCateDetail = () => setMouseInCateDetail(false);
  const onCategoryClick = () => history.push(`/product/${cateCode}`);

  useEffect(() => {
    setOpenCateDetail(mouseInCate || mouseInCateDetail);
  }, [mouseInCate, mouseInCateDetail]);

  return (
    <div className="slideshow-container">
      <Menu className="list-product-container" minimal>
        <div
          onMouseEnter={() => onMouseEnterCate("apple")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<AppleIcon />}
            text="Laptop Apple"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("dell")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<DellIcon />}
            text="Laptop Dell"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("hp")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<HpIcon />}
            text="Laptop HP"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("acer")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<AcerIcon />}
            text="Laptop Acer"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("asus")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<AsusIcon />}
            text="Laptop ASUS"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("lenovo")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<LenovoIcon />}
            text="Laptop Lenovo"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("lg")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<LgIcon />}
            text="Laptop LG"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("msi")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<MsiIcon />}
            text="Laptop MSI"
            onClick={onCategoryClick}
          />
        </div>
        <div
          onMouseEnter={() => onMouseEnterCate("huawei")}
          onMouseLeave={() => onMouseLeaveCate()}
        >
          <MenuItem
            icon={<HuaweiIcon />}
            text="Laptop Huewei"
            onClick={onCategoryClick}
          />
        </div>
      </Menu>
      {/* {openCateDetail && (
        <div
          className="list-detail-container"
          onMouseEnter={onMouseEnterCateDetail}
          onMouseLeave={onMouseLeaveCateDetail}
        >
          <div className="list-detail">
            <div className="list-detail-product-container">className</div>
            <div className="list-detail-image-container">
              <img
                className="list-detail-image"
                src={process.env.PUBLIC_URL + `/images/24.jpg`}
              />
            </div>
          </div>
        </div>
      )} */}
      <div
        className="sub-list-slideshow"
        style={{
          top: "20px",
          backgroundImage: `url("${process.env.PUBLIC_URL}/images/16.png")`,
        }}
      ></div>
      <div
        className="sub-list-slideshow"
        style={{
          top: "150px",
          backgroundImage: `url("${process.env.PUBLIC_URL}/images/17.png")`,
        }}
      ></div>
      <div
        className="sub-list-slideshow"
        style={{
          top: "280px",
          backgroundImage: `url("${process.env.PUBLIC_URL}/images/18.png")`,
        }}
      ></div>
      <AutoplaySlider
        className="slideshow"
        play={true}
        cancelOnInteraction={false}
        interval={4000}
      >
        {[...Array(10).keys()].map((item) => (
          <div data-src={process.env.PUBLIC_URL + `/images/${item}.png`} />
        ))}
      </AutoplaySlider>
    </div>
  );
};

export default MainSlideshow;
