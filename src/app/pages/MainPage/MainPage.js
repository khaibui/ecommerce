import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import Navbar from "../../components/Navbar";
import MainHeader from "../../components/MainHeader";
import Footer from "../../components/Footer";
import SpeedDial from "../../components/SpeedDial";
import ProductList from "../../components/ProductList";

import MainSlideshow from "./MainSlideShow";

import { highlightData } from "../../utils/Constraint";

const MainPage = (props) => {
  const history = useHistory();

  const [hidden, setHidden] = useState(true);
  const [moreFactor, setMoreFactor] = useState(1);

  const onMoreClick = () => setMoreFactor((preState) => preState + 1);
  const onHighLightClick = (cateCodeParam) =>
    history.push(`/product/${cateCodeParam}`);

  window.onscroll = function (ev) {
    if (window.innerHeight + window.scrollY >= 1200) setHidden(false);
    else setHidden(true);
  };

  return (
    <div>
      <Navbar />
      <img className="image" src={process.env.PUBLIC_URL + "/images/10.png"} />
      <div className="container">
        <MainHeader />
        <MainSlideshow />
        <div className="sub-container">
          <img
            className="image-ads"
            src={process.env.PUBLIC_URL + "/images/25.png"}
          />
          <div className="group-container">
            <div className="title">
              <div className="title-mark" />
              <div>Thương hiệu nổi bật</div>
            </div>
            <div className="highlight-container">
              {highlightData.map((item) => (
                <div
                  className="highlight-sub-container"
                  onClick={() => onHighLightClick(item.cateCode)}
                >
                  <img
                    className="highlight"
                    src={process.env.PUBLIC_URL + item.image}
                  />
                  <div className="highlight-title">{item.title}</div>
                  <div className="highlight-script">{item.script}</div>
                </div>
              ))}
            </div>
          </div>

          <div className="group-container">
            <a href="/product/apple" className="product-see-all">
              Xem tất cả
            </a>
            <ProductList moreFactor={moreFactor} />
            <a className="product-see-all" onClick={onMoreClick}>
              More
            </a>
          </div>
        </div>
        <SpeedDial hidden={hidden} />
        <Footer />
      </div>
    </div>
  );
};

export default MainPage;
