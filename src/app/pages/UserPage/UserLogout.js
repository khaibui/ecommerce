import React from "react";
import { Classes, Dialog } from "@blueprintjs/core";
import { useHistory } from "react-router-dom";

import * as FirebaseService from "../../firebase/service";

const UserLogout = (props) => {
  const history = useHistory();

  const onCancelClick = () => props.onClose();
  const onLogoutClick = () => {
    FirebaseService.signOut();
    history.push("/");
  };

  return (
    <Dialog {...props} icon="log-out" title="Đăng xuất">
      <div className={Classes.DIALOG_BODY}>
        <div>Bạn có muốn đăng xuất khỏi tài khoản hiện tại?</div>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <button className="user-cart-button-remove" onClick={onCancelClick}>
            Bỏ qua
          </button>
          <button className="user-profile-button" onClick={onLogoutClick}>
            Đăng xuất
          </button>
        </div>
      </div>
    </Dialog>
  );
};

export default UserLogout;
