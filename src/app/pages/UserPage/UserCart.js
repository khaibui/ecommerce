import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "@blueprintjs/core";
import { Tooltip2 } from "@blueprintjs/popover2";

import Counter from "../../components/Counter";
import ConfirmDialog from "../../components/ConfirmDialog";

import UserPayment from "./UserPayment";

import * as FirebaseService from "../../firebase/service";
import * as Actions from "../../utils/store/action";

const UserCartItem = (props) => {
  const history = useHistory();
  const counterValue = props.currentCartDetail.productTotal;
  const originPrice = props.currentCartDetail.productPrice;
  const hasDiscount = props.currentCartDetail.productDiscount;
  const finalPrice =
    hasDiscount > 0 ? originPrice * (1 - hasDiscount / 100) : originPrice;

  const userData = useSelector(({ User }) => User.User.user);

  const onCheckboxChange = () =>
    props.onChangeCheckBox(props.currentCartDetail);

  const onItemClick = () =>
    history.push(
      `/product-detail/${props.currentCartDetail.cateCode}/${props.currentCartDetail.productCode}`
    );

  return (
    <div className="user-cart-item-row flex flex-row items-center h-40">
      <div className="user-cart-item-check">
        <Button
          minimal
          icon={
            props.currentActive.find(
              (item) => item.productCode === props.currentCartDetail.productCode
            )
              ? "selection"
              : "circle"
          }
          onClick={onCheckboxChange}
        />
      </div>
      <img className="h-40" src={props.currentCartDetail.image[0]} />
      <div className="user-cart-item-name" onClick={onItemClick}>
        <p className="user-cart-item-title">
          {props.currentCartDetail.productTitle}
        </p>
        <p className="user-cart-item-code">{`SKU: ${props.currentCartDetail.productCode}`}</p>
      </div>
      <div className="user-cart-item-counter">
        <Counter
          counterValue={counterValue}
          currentList={props.currentCart}
          cartDetail={props.currentCartDetail}
          userData={userData}
        />
      </div>
      <div className="user-cart-item-price">
        <p className="user-cart-item-final">{`${(
          finalPrice * counterValue
        ).toLocaleString()}đ`}</p>
        <p className="detail-info-price-origin">{`${(
          originPrice * counterValue
        ).toLocaleString()}đ`}</p>
      </div>
    </div>
  );
};

const UserCartHead = (props) => {
  const dispatch = useDispatch();

  const onRemoveClick = () => {
    dispatch(
      Actions.openDialog(
        "Xác nhận xóa sản phẩm",
        "Bạn có chắc chắn muốn xóa sản phẩm khỏi giỏ hàng. Bạn có thể mua lại sản phẩm nếu cần thiết ở mục sản phẩm.",
        () => {
          props.onRemove();
        },
        "cross"
      )
    );
  };

  return (
    <div {...props} className="user-cart-item-head-container">
      <ConfirmDialog />
      <div className="user-cart-item-check">
        <Tooltip2
          content={
            props.currentActive.length === props.currentCartDetail.length
              ? "Bỏ chọn tất cả"
              : "Chọn tất cả"
          }
          placement="bottom"
        >
          <Button
            minimal
            icon={
              props.currentActive.length === props.currentCartDetail.length
                ? "selection"
                : "circle"
            }
            onClick={props.onAllCheckChange}
          />
        </Tooltip2>
      </div>
      <div className="user-cart-item-button-group">
        <button className="user-cart-button-remove" onClick={onRemoveClick}>
          Xóa
        </button>
        <button
          className="user-profile-button"
          disabled={!props.currentActive.length}
          onClick={props.onPayment}
        >
          Đặt hàng
        </button>
      </div>
    </div>
  );
};

const UserCart = (props) => {
  const userCartData = Array.isArray(props.user.userCart)
    ? props.user.userCart
    : [];
  const userCartDataMapping = userCartData.map((item) => ({
    ...props.product.find((prod) => prod.productCode === item.productCode),
    ...item,
  }));

  const [paymentOpen, setPaymentOpen] = useState(false);
  const [itemActive, setItemActive] = useState([]);

  const onPaymentClick = () => setPaymentOpen(true);
  const onPaymentClose = () => setPaymentOpen(false);

  const onRemoveClick = async () => {
    const updateCart = userCartData.filter(
      (item) => !itemActive.find((el) => el.productCode === item.productCode)
    );
    await FirebaseService.updateOneDataFromStore("users", props.user.userId, {
      userCart: updateCart || [],
    });
  };

  const onAllCheckChange = () => {
    if (itemActive.length === userCartDataMapping.length) setItemActive([]);
    else setItemActive([...userCartDataMapping]);
  };

  const onChangeCheckBox = (itemDetailParam) => {
    const itemExisted = itemActive.find(
      (item) => item.productCode === itemDetailParam.productCode
    );
    const updateActiveList = itemExisted
      ? itemActive.filter(
          (item) => item.productCode !== itemDetailParam.productCode
        )
      : [...itemActive, itemDetailParam];
    setItemActive(updateActiveList);
  };

  return (
    <div {...props} className="user-profile-container">
      <p className="user-profile-title">Giỏ hàng của bạn</p>
      <div className="user-cart-content-container">
        <UserCartHead
          currentActive={itemActive}
          currentCartDetail={userCartDataMapping}
          onAllCheckChange={onAllCheckChange}
          onPayment={onPaymentClick}
          onRemove={onRemoveClick}
        />
        {userCartDataMapping.map((item) => (
          <UserCartItem
            userData={props.user}
            currentCart={userCartData}
            currentCartDetail={item}
            currentActive={itemActive}
            onChangeCheckBox={onChangeCheckBox}
          />
        ))}
      </div>
      <UserPayment
        isOpen={paymentOpen}
        onClose={onPaymentClose}
        userData={props.user}
        currentCart={userCartData}
        curentCartDetail={userCartDataMapping}
        currentActive={itemActive}
      />
    </div>
  );
};

export default UserCart;
