import React, { useState } from "react";
import { Classes, Dialog, InputGroup, Button } from "@blueprintjs/core";

const OldAccount = (props) => {
  const [showPassword, setShowPassword] = useState(false);

  const onShowPasswordClick = () => setShowPassword(!showPassword);

  return (
    <div {...props}>
      <InputGroup
        large
        placeholder="Tên đăng nhập"
        className="user-change-pass-old-account"
      />
      <InputGroup
        large
        type={showPassword ? "text" : "password"}
        placeholder="Mật khẩu hiện tại"
        className="user-change-pass-old-account"
        rightElement={
          <Button
            minimal
            icon={showPassword ? "eye-open" : "eye-off"}
            onClick={onShowPasswordClick}
          />
        }
      />
    </div>
  );
};

const NewAccount = (props) => {
  const [showPassword, setShowPassword] = useState(false);

  const onShowPasswordClick = () => setShowPassword(!showPassword);

  return (
    <div {...props}>
      <InputGroup
        large
        type={showPassword ? "text" : "password"}
        placeholder="Mật khẩu mới"
        className="user-change-pass-old-account"
        rightElement={
          <Button
            minimal
            icon={showPassword ? "eye-open" : "eye-off"}
            onClick={onShowPasswordClick}
          />
        }
      />
      <InputGroup
        large
        type={showPassword ? "text" : "password"}
        placeholder="Xác nhận mật khẩu"
        className="user-change-pass-old-account"
        rightElement={
          <Button
            minimal
            icon={showPassword ? "eye-open" : "eye-off"}
            onClick={onShowPasswordClick}
          />
        }
      />
    </div>
  );
};

const UserLogout = (props) => {
  const [authSuccess, setAuthSuccess] = useState(false);

  const onCancelClick = () => {
    setAuthSuccess(false);
    props.onClose();
  };
  const onLogoutClick = () => setAuthSuccess(true);

  return (
    <Dialog {...props} icon="log-out" title="Thay đổi mật khẩu">
      <div className={Classes.DIALOG_BODY}>
        {authSuccess ? <NewAccount /> : <OldAccount />}
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <button className="user-cart-button-remove" onClick={onCancelClick}>
            Bỏ qua
          </button>
          <button className="user-profile-button" onClick={onLogoutClick}>
            {authSuccess ? "Xác nhận" : "Tiếp theo"}
          </button>
        </div>
      </div>
    </Dialog>
  );
};

export default UserLogout;
