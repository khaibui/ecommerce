import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { Menu, MenuDivider, MenuItem } from "@blueprintjs/core";

import ConfirmDialog from "../../components/ConfirmDialog";

import * as Actions from "../../utils/store/action";
import * as FirebaseService from "../../firebase/service";

const labelPage = {
  wait: "Đơn hàng chờ xác nhận",
  confirm: "Đơn hàng đã xác nhận",
  tranfer: "Đơn hàng đang vận chuyển",
  success: "Đơn hàng đã nhận",
  cancel: "Đơn hàng đã hủy",
};

const UserBillItem = (props) => {
  const productTotal = props.billProduct.productTotal;
  const productOriginPrice = props.billProduct.productPrice * productTotal;
  const productFinalPrice = props.billProduct.productFinalPrice * productTotal;

  return (
    <React.Fragment>
      <div className="user-bill-item-container flex flex-row items-center h-40">
        <img className="h-40" src={props.billProduct.image[0]} />
        <div className="user-cart-item-name">
          <p className="user-cart-item-title">
            {props.billProduct.productTitle}
          </p>
          <p className="user-cart-item-code">{`SKU: ${props.billProduct.productCode}`}</p>
        </div>
        <div className="user-cart-item-counter user-bill-item-counter">
          {`Số lượng: ${productTotal}`}
        </div>
        <div className="user-cart-item-counter">
          <img
            className="product-list-item-freeship-icon"
            src={process.env.PUBLIC_URL + "/assets/free-ship.png"}
          />
        </div>
        <div className="user-cart-item-price">
          <p className="user-cart-item-final">{`${productFinalPrice?.toLocaleString()}đ`}</p>
          <p className="detail-info-price-origin">{`${productOriginPrice?.toLocaleString()}đ`}</p>
        </div>
      </div>
    </React.Fragment>
  );
};

const UserBill = (props) => {
  const dispatch = useDispatch();

  const [billData, setBillData] = useState([]);

  const onCancelClick = (bill) => {
    dispatch(
      Actions.openDialog(
        "Xác nhận hủy đơn",
        "Bạn vui lòng cân nhắc trước khi xác nhận hủy đơn.",
        () => {
          const updateBillStatus = props.user.userBill.map((item) =>
            item.billStatus === bill.billStatus
              ? { ...bill, billStatus: "cancel" }
              : item
          );
          FirebaseService.updateOneDataFromStore("users", props.user.userId, {
            userBill: updateBillStatus,
          });
        },
        "cross"
      )
    );
  };

  useEffect(() => {
    if (props.user && props.phase) {
      const billRenderData =
        props.user.userBill &&
        props.user.userBill.filter((item) => item.billStatus === props.phase);
      setBillData(billRenderData);
    }
  }, [props.user]);

  return (
    <div {...props} className="user-profile-container">
      <ConfirmDialog />
      <p className="user-profile-title">{labelPage[props.phase]}</p>
      {billData.map((bill) => {
        return (
          <div className="user-bill-content-container">
            {props.phase !== "cancel" && (
              <div className="w-full flex justify-end items-end">
                <button
                  className="user-cart-button-remove"
                  onClick={() => onCancelClick(bill)}
                >
                  Hủy đơn
                </button>
              </div>
            )}
            {bill.billProduct.map((product) => (
              <UserBillItem billProduct={product} />
            ))}
            <Menu minimal className="user-bill-menu-container">
              <MenuDivider />
              <MenuItem
                icon="barcode"
                text="Mã đơn hàng"
                label={bill.billCode}
              />
              <MenuItem
                icon="history"
                text="Ngày đặt hàng"
                label={bill.billCreatedAt}
              />
              <MenuItem icon="cycle" text="Phí vận chuyển" label={0} />
              <MenuDivider />
              <MenuItem
                icon="dollar"
                text="Tổng giá trị đơn hàng"
                label={`${bill.billTotal?.toLocaleString()}đ`}
              />
            </Menu>
          </div>
        );
      })}
    </div>
  );
};

export default UserBill;
