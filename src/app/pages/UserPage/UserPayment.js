import React, { useState } from "react";
import {
  Classes,
  Dialog,
  Menu,
  MenuDivider,
  MenuItem,
  RadioGroup,
  Radio,
} from "@blueprintjs/core";

import * as FirebaseService from "../../firebase/service";

const UserPaymentItem = (props) => {
  const finalPrice =
    props.cartDetail.productPrice *
    ((100 - props.cartDetail.productDiscount) / 100);
  return (
    <div className="user-payment-item-container">
      <img
        className="user-payment-item-image"
        src={props.cartDetail.image[0]}
      />
      <div className="user-payment-item-name">
        {props.cartDetail.productTitle}
      </div>
      <div className="user-payment-item-counter">
        {props.cartDetail.productTotal}
      </div>
      <div className="user-payment-item-price">
        {`${(finalPrice * props.cartDetail.productTotal).toLocaleString()}đ`}
      </div>
    </div>
  );
};

const UserPayment = (props) => {
  const totalOriginFee = props.currentActive
    .map((item) => item.productTotal * item.productPrice)
    .reduce((a, b) => a + b, 0);

  const totalFinalFee = props.currentActive
    .map(
      (item) =>
        item.productTotal *
        item.productPrice *
        ((100 - item.productDiscount) / 100)
    )
    .reduce((a, b) => a + b, 0);

  const totalSaleFee = totalOriginFee - totalFinalFee;

  const [payType, setPayType] = useState("normal");

  const onPaymentClick = async () => {
    const updateCart = props.currentCart.filter(
      (item) =>
        !props.currentActive.find((el) => el.productCode === item.productCode)
    );
    const updateCartUser = updateCart.length ? updateCart : "null";
    const extractToProductBill = props.currentActive.map((item) => ({
      ...item,
      productFinalPrice:
        item.productPrice * ((100 - item.productDiscount) / 100),
    }));
    const createdAt = new Date();
    const extractToBill = {
      billCode: createdAt.getTime(),
      billCreatedAt: `${createdAt.getDate()}/${
        createdAt.getMonth() + 1
      }/${createdAt.getFullYear()}`,
      billTotal: totalFinalFee,
      billStatus: "wait",
      billProduct: extractToProductBill,
      billPayment: payType === "normal" ? "no" : "yes",
      billPaymentType: payType,
    };
    const updateBillUser = Array.isArray(props.userData.userBill)
      ? [...props.userData.userBill, extractToBill]
      : [extractToBill];

    await FirebaseService.updateOneDataFromStore(
      "users",
      props.userData.userId,
      {
        userCart: updateCartUser,
        userBill: updateBillUser,
      }
    );
  };

  return (
    <Dialog {...props} icon="credit-card" title="Thanh toán mua hàng">
      <div className={Classes.DIALOG_BODY}>
        <Menu minimal className="user-payment-menu-container">
          {props.currentActive.map((item) => (
            <UserPaymentItem
              cartDetail={item}
              cartList={props.currentCart}
              totalFinalFee={totalFinalFee}
            />
          ))}
          <MenuItem
            icon="mobile-phone"
            text="Điện thoại nhận hàng"
            label={props.userData.userProfile.userPhone}
          />
          <MenuItem
            icon="map-marker"
            text="Địa chỉ nhận hàng"
            label={props.userData.userProfile.userAddress}
          />
          <MenuDivider />
          <MenuItem
            icon="dollar"
            text="Tạm tính"
            label={`${totalOriginFee.toLocaleString()}đ`}
          />
          <MenuItem icon="cycle" text="Phí vận chuyển" label="0đ" />
          <MenuItem
            icon="tag"
            text="Khuyến mãi"
            label={`${totalSaleFee.toLocaleString()}đ`}
          />
          <MenuDivider />
          <MenuItem label={`${totalFinalFee.toLocaleString()}đ`} />
        </Menu>
        <RadioGroup
          label="Lựa chọn phương thức thanh toán"
          name="group"
          onChange={(e) => setPayType(e.target.value)}
          selectedValue={payType}
        >
          <Radio label="Thanh toán khi nhận hàng" value="normal" />
          <Radio label="Thanh toán sử dụng Zalo Pay" value="zalo" />
        </RadioGroup>
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <button className="user-profile-button" onClick={onPaymentClick}>
            Xác nhận
          </button>
        </div>
      </div>
    </Dialog>
  );
};

export default UserPayment;
