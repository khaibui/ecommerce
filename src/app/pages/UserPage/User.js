import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Navbar from "../../components/Navbar";
import MainHeader from "../../components/MainHeader";
import Footer from "../../components/Footer";

import UserMenu from "./UserMenu";
import UserProfile from "./UserProfile";
import UserCart from "./UserCart";
import UserBill from "./UserBill";
import UserNotFound from "./UserNotFound";

const UserPage = (props) => {
  const params = useParams();
  const currentPage = params.page;

  const userData = useSelector(({ User }) => User.User.user);

  const productData = useSelector(({ User }) => User.User.products);

  return (
    <div {...props}>
      <Navbar />
      <img className="image" src={process.env.PUBLIC_URL + "/images/10.png"} />
      <div className="container">
        <MainHeader />
        <div className="user-sub-container">
          <UserMenu active={currentPage} user={userData} />
          {currentPage === "profile" && (
            <UserProfile user={userData} product={productData} />
          )}
          {currentPage === "cart" &&
            (Array.isArray(userData?.userCart) ? (
              <UserCart user={userData} product={productData} />
            ) : (
              <UserNotFound />
            ))}
          {currentPage === "bill" &&
            (Array.isArray(userData?.userBill) ? (
              <UserBill user={userData} product={productData} />
            ) : (
              <UserNotFound />
            ))}
          {["wait", "confirm", "tranfer", "success", "cancel"].includes(
            currentPage
          ) &&
            (Array.isArray(userData?.userBill) ? (
              <UserBill
                user={userData}
                product={productData}
                phase={currentPage}
              />
            ) : (
              <UserNotFound />
            ))}
        </div>
        <Footer />
      </div>
    </div>
  );
};

export default UserPage;
