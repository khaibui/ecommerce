import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Menu, MenuDivider, MenuItem } from "@blueprintjs/core";

import UserLogout from "./UserLogout";
import UserChangePassword from "./UserChangePassword";

const labelPage = {
  wait: "Đơn hàng chờ xác nhận",
  confirm: "Đơn hàng xác nhận",
  tranfer: "Đơn hàng đang vận chuyển",
  success: "Đơn hàng đã nhận",
  cancel: "Đơn hàng đã hủy",
};

const UserMenu = (props) => {
  const history = useHistory();

  const [logoutOpen, setLogoutOpen] = useState(false);
  const [changePassOpen, setChangePassOpen] = useState(false);
  const [labelQuantiy, setLabelQuantity] = useState({});

  const onMenuItemClick = (type) => history.push(`/user/${type}`);
  const onLogoutClick = () => setLogoutOpen(true);
  const onLogoutClose = () => setLogoutOpen(false);
  const onChangePassClick = () => setChangePassOpen(true);
  const onChangePassClose = () => setChangePassOpen(false);

  const renderMenuItemProps = (iconParam, textParam, typeParam) => {
    let labelNumberDisplay = "";
    if (typeParam === "cart" && Array.isArray(props.user?.userCart))
      labelNumberDisplay = props.user.userCart.length;
    if (
      Object.keys(labelPage).includes(typeParam) &&
      labelQuantiy[typeParam] > 0
    )
      labelNumberDisplay = labelQuantiy[typeParam];
    return {
      className: "user-menu-item",
      icon: iconParam,
      text: textParam,
      label: labelNumberDisplay,
      active: props.active === typeParam,
      onClick() {
        onMenuItemClick(typeParam);
      },
    };
  };

  useEffect(() => {
    if (props.user && Array.isArray(props.user.userBill)) {
      const countQuantity = Object.keys(labelPage).reduce((obj, key) => {
        const numberOfItem = props.user.userBill.filter(
          (item) => item.billStatus === key
        );
        return { ...obj, [key]: numberOfItem.length };
      }, {});
      setLabelQuantity(countQuantity);
    }
  }, [props.user]);

  return (
    <div className="user-menu-container">
      <Menu minimal className="user-menu">
        <MenuItem
          {...renderMenuItemProps("user", "Thông tin tài khoản", "profile")}
        />
        <MenuItem
          {...renderMenuItemProps("shopping-cart", "Quản lý giỏ hàng", "cart")}
        />
        <MenuDivider />
        <MenuItem
          {...renderMenuItemProps("star", "Đơn hàng chờ xác nhận", "wait")}
        />
        <MenuItem
          {...renderMenuItemProps("confirm", "Đơn hàng đã xác nhận", "confirm")}
        />
        <MenuItem
          {...renderMenuItemProps("box", "Đơn hàng đang vận chuyển", "tranfer")}
        />
        <MenuItem
          {...renderMenuItemProps("endorsed", "Đơn hàng đã nhận", "success")}
        />
        <MenuItem
          {...renderMenuItemProps("trash", "Đơn hàng đã hủy", "cancel")}
        />
        <MenuDivider />
        <MenuItem
          {...renderMenuItemProps("issue", "Thay đổi mật khẩu", null)}
          onClick={() => onChangePassClick()}
        />
        <MenuItem
          {...renderMenuItemProps("log-out", "Đăng xuất", null)}
          onClick={() => onLogoutClick()}
        />
      </Menu>
      <UserLogout isOpen={logoutOpen} onClose={onLogoutClose} />
      <UserChangePassword isOpen={changePassOpen} onClose={onChangePassClose} />
    </div>
  );
};

export default UserMenu;
