import React, { useState } from "react";
import { InputGroup } from "@blueprintjs/core";

import Snackbar from "../../components/Snackbar";

import * as FirebaseService from "../../firebase/service";

const UserProfile = (props) => {
  const userProfile = props.user?.userProfile;

  const [warningShow, setWarningShow] = useState(false);
  const [fullNameText, setFullNameText] = useState(userProfile.userFullName);
  const [phoneText, setPhoneText] = useState(userProfile.userPhone);
  const [addressText, setAddressText] = useState(userProfile.userAddress);
  const [snackbarDetail, setSnackbarDetail] = useState({
    open: false,
    type: "success",
    text: "",
  });

  const onUpdateSnackbar = (openParam, typeParam, textParam) =>
    setSnackbarDetail((snackbarDetail) => ({
      ...snackbarDetail,
      open: openParam,
      type: typeParam,
      text: textParam,
    }));

  const validateProfileForm = () => {
    const fillFullForm =
      fullNameText.length && phoneText.length && addressText.length;
    if (!fillFullForm)
      onUpdateSnackbar(
        true,
        "warning",
        "Bạn vui lòng kiểm tra đầy đủ thông tin"
      );
    setWarningShow(!fillFullForm);
    return fillFullForm;
  };

  const onFullNameChange = (e) => setFullNameText(e.target.value);
  const onPhoneChange = (e) => setPhoneText(e.target.value);
  const onAddressChange = (e) => setAddressText(e.target.value);
  const onSnackbarClose = () => onUpdateSnackbar(false, "", "");

  const onUpdateProfileClick = () => {
    if (validateProfileForm()) {
      const profileUpdatedForm = {
        userFullName: fullNameText,
        userPhone: phoneText,
        userEmail: userProfile.userEmail,
        userAddress: addressText,
      };
      FirebaseService.updateOneDataFromStore("users", props.user.userId, {
        userProfile: profileUpdatedForm,
      });
      onUpdateSnackbar(true, "success", "Cập nhật hoàn tất");
    }
  };

  return (
    <div {...props} className="user-profile-container">
      <p className="user-profile-title">Thông tin tài khoản</p>
      <div className="user-profile-content-container">
        <InputGroup
          leftIcon="font"
          placeholder="Họ và tên"
          className="user-profile-input"
          value={fullNameText}
          onChange={onFullNameChange}
          intent={warningShow && !fullNameText.length ? "danger" : "none"}
        />
        <InputGroup
          disabled
          leftIcon="envelope"
          placeholder="Email"
          className="user-profile-input"
          value={userProfile.userEmail}
        />
        <InputGroup
          type="number"
          leftIcon="mobile-phone"
          placeholder="Số điện thoại"
          className="user-profile-input"
          value={phoneText}
          onChange={onPhoneChange}
          intent={warningShow && !phoneText.length ? "danger" : "none"}
        />
        <InputGroup
          leftIcon="map-marker"
          placeholder="Địa chỉ giao hàng"
          className="user-profile-input"
          value={addressText}
          onChange={onAddressChange}
          intent={warningShow && !addressText.length ? "danger" : "none"}
        />
        <button className="user-profile-button" onClick={onUpdateProfileClick}>
          Cập nhật
        </button>
        <Snackbar
          open={snackbarDetail.open}
          type={snackbarDetail.type}
          text={snackbarDetail.text}
          onClose={onSnackbarClose}
        />
      </div>
    </div>
  );
};

export default UserProfile;
