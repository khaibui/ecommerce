import React from "react";

const UserNotFound = (props) => {
  return (
    <div {...props} className="user-sale-container">
      <img src={process.env.PUBLIC_URL + "/assets/no-product.png"} />
    </div>
  );
};

export default UserNotFound;
