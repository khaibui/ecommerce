import React from "react";
import { useHistory } from "react-router-dom";
import { NonIdealState, Button } from "@blueprintjs/core";
import Navbar from "../../components/Navbar";
import Footer from "../../components/Footer";

const Error404 = () => {
  const description = (
    <div>
      Your search didn't match any files.
      <br />
      Try searching for something else.
    </div>
  );

  const history = useHistory();

  const onBackClick = () => history.push("/");

  return (
    <div>
      <Navbar />
      <img className="image" src={process.env.PUBLIC_URL + "/images/10.png"} />
      <div className="error-container">
        <NonIdealState
          icon="search"
          title="404 error page"
          description={description}
          action={
            <Button
              text="Back to home"
              minimal
              intent="success"
              icon="arrow-left"
              onClick={onBackClick}
            />
          }
        />
      </div>
      <Footer />
    </div>
  );
};

export default Error404;
