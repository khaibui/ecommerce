import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Icon } from "@blueprintjs/core";
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Button,
  Backdrop,
  CircularProgress,
} from "@material-ui/core";

import { ADMIN_ACCOUNT } from "../../../utils/Constraint";

import UserDialog from "./UserDialog";

import * as FirebaseService from "../../../firebase/service";
import * as Actions from "../../../utils/store/action";
import * as Functions from "../../../utils/CommonFunction";

const headerConstraint = [
  {
    id: "email",
    label: "Địa chỉ email",
  },
  {
    id: "fullName",
    label: "Tên người dùng",
  },
  {
    id: "phone",
    label: "Số điện thoại",
    icon: "phone",
  },
  {
    id: "address",
    label: "Địa chỉ",
    icon: "ip-address",
  },
  {
    id: "currentCart",
    label: "Giỏ sản phẩm",
    icon: "shopping-cart",
  },
  {
    id: "sucess",
    label: "Đã mua",
    icon: "automatic-updates",
  },
  {
    id: "fail",
    label: "Đã hủy",
    icon: "cross",
  },
  {
    id: "status",
    label: "Trạng thái",
    icon: "exchange",
  },
];

const UserTable = (props) => {
  const dispatch = useDispatch();
  const classes = Functions.useStyles();

  const allUsers = useSelector(({ User }) => User.User.allUsers);

  const [backDropOpen, setBackDropOpen] = useState(false);
  const [reload, setReload] = useState(false);
  const [selected, setSelected] = useState(null);
  const [openDialog, setOpenDialog] = useState(false);

  const onActiveChange = async (label) => {
    if (selected && selected.userId) {
      await FirebaseService.updateOneDataFromStore("users", selected.userId, {
        status: label,
      });
      setReload(!reload);
    }
  };

  useEffect(async () => {
    setBackDropOpen(true);
    await FirebaseService.getALlDataFromStore("users").then(
      async (snapshot) => {
        const dataSnapshot = snapshot.docs.map((item) => ({ ...item.data() }));
        await dispatch(Actions.setAllUserAccount(dataSnapshot));
      }
    );
    setBackDropOpen(false);
  }, [reload]);

  return (
    <React.Fragment>
      <Backdrop className={classes.backdrop} open={backDropOpen}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <div
        className="w-full absolute top-14 overflow-auto"
        style={{ maxHeight: "calc(100vh - 120px)" }}
      >
        <Table
          stickyHeader
          className="w-full cursor-pointer h-full"
          size="small"
        >
          <TableHead>
            <TableRow>
              {headerConstraint.map((headerItem) => (
                <TableCell
                  key={headerItem.id}
                  className="text-left table-cell "
                >
                  <div className="w-full flex flex-row items-center">
                    <Icon color="#aaa" icon={headerItem.icon} />
                    <div className="ml-2">{headerItem.label}</div>
                  </div>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {allUsers
              .filter((item) => !ADMIN_ACCOUNT.includes(item.userEmail))
              .map((item) => (
                <TableRow
                  hover
                  selected={selected && item.userEmail === selected.userEmail}
                  onClick={() => setSelected(item)}
                >
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">{item.userEmail}</div>
                  </TableCell>
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">
                      {item.userProfile.userFullName}
                    </div>
                  </TableCell>
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">
                      {item.userProfile.userPhone}
                    </div>
                  </TableCell>
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">
                      {item.userProfile.userAddress}
                    </div>
                  </TableCell>
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">
                      {(item.userCart &&
                        Array.isArray(item.userCart) &&
                        item.userCart.length) ||
                        0}
                    </div>
                  </TableCell>
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">
                      {item.userBill?.length || 0}
                    </div>
                  </TableCell>
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">
                      {item.userBill && Array.isArray(item.userBill)
                        ? item.userBill.filter(
                            (bill) => bill.billStatus === "cancel"
                          ).length
                        : 0}
                    </div>
                  </TableCell>
                  <TableCell style={{ width: "12.5%" }}>
                    <div className="truncate w-32">{item.status}</div>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </div>

      <div className="w-full absolute min-h-20 p-3 bottom-0 flex items-end">
        <Button onClick={() => setOpenDialog(true)}>Thêm tài khoản</Button>
        <Button disabled={!selected} onClick={() => onActiveChange("Disabled")}>
          Khóa tài khoản
        </Button>
        <Button disabled={!selected} onClick={() => onActiveChange("Active")}>
          Khôi phục tài khoản
        </Button>
        <Button disabled={!selected} onClick={() => onActiveChange("Deleted")}>
          Xóa tài khoản
        </Button>
      </div>

      <UserDialog
        open={openDialog}
        handleClose={() => setOpenDialog(false)}
        reload={() => setReload(!reload)}
      />
    </React.Fragment>
  );
};

export default UserTable;
