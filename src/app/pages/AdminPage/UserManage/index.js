import React from "react";
import { Container } from "@material-ui/core";

import UserTable from "./UserTable";

const UserManage = () => {
  return <UserTable />;
};

export default UserManage;
