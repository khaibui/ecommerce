import React, { useState } from "react";
import { Button, Backdrop, CircularProgress } from "@material-ui/core";
import { Classes, Dialog } from "@blueprintjs/core";

import Snackbar from "../../../components/Snackbar";

import * as FirebaseService from "../../../firebase/service";
import * as Functions from "../../../utils/CommonFunction";

const UserDialog = (props) => {
  const classes = Functions.useStyles();

  const [backDropOpen, setBackDropOpen] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [name, setName] = useState("");
  const [snackbarDetail, setSnackbarDetail] = useState({
    open: false,
    type: "success",
    text: "",
  });

  const onUpdateSnackbar = (openParam, typeParam, textParam) =>
    setSnackbarDetail((snackbarDetail) => ({
      ...snackbarDetail,
      open: openParam,
      type: typeParam,
      text: textParam,
    }));

  const onCancelClick = () => {
    setEmail("");
    setPassword("");
    setPhone("");
    setAddress("");
    setName("");
    props.handleClose();
  };

  const onCreateClick = async () => {
    if (
      email.length &&
      password.length &&
      phone.length &&
      address.length &&
      name.length
    ) {
      setBackDropOpen(true);
      await FirebaseService.createUserByEmailPassword(email, password)
        .then(async (userCredential) => {
          if (userCredential) {
            const uid = userCredential.user.uid;
            const email = userCredential.user.email;
            const userInitial = {
              status: "Active",
              userId: uid,
              userEmail: email,
              userProfile: {
                userFullName: name,
                userEmail: email,
                userPhone: phone,
                userAddress: address,
              },
            };
            await FirebaseService.createUserToStore("users", uid, userInitial);
          }
        })
        .catch((err) => onUpdateSnackbar(true, "warning", err.message));
      props.handleClose();
      props.reload();
    } else onUpdateSnackbar(true, "warning", "Thiếu thông tin cần thiết");

    setBackDropOpen(false);
  };

  return (
    <React.Fragment>
      <Backdrop className={classes.backdrop} open={backDropOpen}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Dialog
        isOpen={props.open}
        onClose={props.handleClose}
        icon="new-person"
        title="Thêm tài khoản"
      >
        <div className={Classes.DIALOG_BODY}>
          <label class="bp3-label ">
            Địa chỉ email
            <input
              class="bp3-input bp3-fill"
              dir="auto"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </label>
          <label class="bp3-label ">
            Tên người dùng
            <input
              class="bp3-input bp3-fill"
              dir="auto"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </label>
          <label class="bp3-label ">
            Mật khẩu
            <input
              class="bp3-input bp3-fill"
              dir="auto"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <label class="bp3-label ">
            Số điện thoại
            <input
              class="bp3-input bp3-fill"
              dir="auto"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
            />
          </label>
          <label class="bp3-label ">
            Địa chỉ giao hàng
            <input
              class="bp3-input bp3-fill"
              dir="auto"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </label>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button onClick={onCancelClick} color="default">
              Bỏ qua
            </Button>
            <Button onClick={onCreateClick}>Xác nhận</Button>
          </div>
        </div>
      </Dialog>
      <Snackbar
        open={snackbarDetail.open}
        type={snackbarDetail.type}
        text={snackbarDetail.text}
        onClose={() => onUpdateSnackbar(false, "", "")}
      />
    </React.Fragment>
  );
};

export default UserDialog;
