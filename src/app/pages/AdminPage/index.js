import React from "react";

import AdminNavbar from "../../components/AdminNavbar";

const AdminPage = () => {
  return (
    <div>
      <AdminNavbar />
    </div>
  );
};

export default AdminPage;
