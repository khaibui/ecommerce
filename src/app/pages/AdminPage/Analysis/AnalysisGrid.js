import React, { useState, useEffect } from "react";
import { Grid } from "@material-ui/core";
import { Line, Bar } from "react-chartjs-2";

import { filterEngine } from "../../../utils/Constraint";

import * as FirebaseService from "../../../firebase/service";

const COLOR = [
  "rgba(255, 99, 132, 0.5)",
  "rgba(54, 162, 235, 0.5)",
  "rgba(255, 206, 86, 0.5)",
  "rgba(75, 192, 192, 0.5)",
  "rgba(153, 102, 255, 0.5)",
  "rgba(255, 159, 64, 0.5)",
  "rgba(123, 58, 71, 0.5)",
  "rgba(59, 86, 156, 0.5)",
  "rgba(17, 99, 206, 0.5)",
];

const BORDER = [
  "rgba(255, 99, 132)",
  "rgba(54, 162, 235)",
  "rgba(255, 206, 86)",
  "rgba(75, 192, 192)",
  "rgba(153, 102, 255)",
  "rgba(255, 159, 64)",
  "rgba(123, 58, 71)",
  "rgba(59, 86, 156)",
  "rgba(17, 99, 206)",
];

const STATUS = {
  wait: "Chờ xác nhận",
  confirm: "Xác nhận",
  tranfer: "Vận chuyển",
  success: "Đã nhận",
  cancel: "Đã hủy",
};

const Analysis = () => {
  const [productChart, setProductChart] = useState([]);
  const [speedProductChart, setSpeedProductChart] = useState([]);
  const [statusChart, setStatusChart] = useState([]);
  const [speedStatus, setSpeedStatusChart] = useState([]);

  const [productDataChart, setProductDataChart] = useState({});

  useEffect(async () => {
    await FirebaseService.getALlDataFromStore("products").then(
      async (snapshot) => {
        const dataSnapshot = snapshot.docs.map((item) => ({
          ...item.data(),
        }));
        const flatProduct = filterEngine[0].detail.map((item) => {
          const countProduct = dataSnapshot.filter(
            (prod) => prod.cateCode === item.code
          ).length;
          return { ...item, count: countProduct };
        });
        setProductChart(flatProduct);
      }
    );
    await FirebaseService.getALlDataFromStore("users").then(
      async (snapshot) => {
        const dataSnapshot = snapshot.docs.map((item) => ({
          ...item.data(),
        }));
        const flatPoduct = dataSnapshot.flatMap((item) => {
          const productInBill = item.userBill?.flatMap((bill) =>
            bill.billProduct.map((prod) => ({
              ...prod,
              billCreatedAt: bill.billCreatedAt,
              billStatus: bill.billStatus,
            }))
          );
          return productInBill || [];
        });

        const speedProduct = filterEngine[0].detail.map((item) => {
          const byCate = flatPoduct.flatMap((prod) =>
            prod.cateCode === item.code
              ? [prod.billCreatedAt.split("/")[1]]
              : []
          );
          const byMonth = [...Array(12).keys()].map(
            (month) =>
              byCate.filter((prod) => month + 1 === parseInt(prod)).length
          );
          return { ...item, byMonth: byMonth };
        });

        const speedPhase = Object.keys(STATUS).map((item) => {
          const byPhase = flatPoduct.flatMap((prod) =>
            prod.billStatus === item ? [prod.billCreatedAt.split("/")[1]] : []
          );
          const byMonth = [...Array(12).keys()].map(
            (month) =>
              byPhase.filter((phase) => month + 1 === parseInt(phase)).length
          );
          return { name: STATUS[item], byMonth: byMonth };
        });

        const statusProduct = filterEngine[0].detail.map((item) => {
          const byStatus = flatPoduct.flatMap((prod) =>
            prod.cateCode === item.code ? [prod.billStatus] : []
          );
          const byPhase = Object.keys(STATUS).map(
            (phase) => byStatus.filter((status) => status === phase).length
          );
          return { ...item, byPhase: byPhase };
        });

        setSpeedStatusChart(speedPhase);
        setStatusChart(statusProduct);
        setSpeedProductChart(speedProduct);
      }
    );
  }, []);

  useEffect(() => {
    if (
      speedProductChart.length &&
      productChart.length &&
      statusChart.length &&
      speedStatus.length
    ) {
      setProductDataChart({
        product: productChart,
        productSpeed: speedProductChart,
        productStatus: statusChart,
        statusSpeed: speedStatus,
      });
    }
  }, [productChart, speedProductChart, statusChart, speedStatus]);

  return (
    <div
      className="w-full absolute top-14 overflow-hidden"
      style={{ height: "calc(100vh - 60px)" }}
    >
      <Grid container spacing={3}>
        <Grid
          item
          xs={6}
          style={{ height: "calc((100vh - 60px)/2)" }}
          className="flex justify-center items-center"
        >
          <div className="w-10/12">
            <Bar
              data={{
                labels:
                  productDataChart.product?.map((item) => item.name) || [],
                datasets: [
                  {
                    data:
                      productDataChart.product?.map((item) => item.count) || [],
                    backgroundColor: COLOR,
                  },
                ],
              }}
              options={{
                responsive: true,
                plugins: {
                  legend: {
                    display: false,
                    position: "top",
                  },
                  title: {
                    display: true,
                    text: "Biểu đồ các sản phẩm",
                  },
                },
              }}
            />
          </div>
        </Grid>
        <Grid
          item
          xs={6}
          style={{ height: "calc((100vh - 60px)/2)" }}
          className="flex justify-center items-center"
        >
          <div className="w-10/12">
            <Line
              data={{
                labels: [...Array(12).keys()].map((item) => `T${item + 1}`),
                datasets: productDataChart.productSpeed?.map((item, index) => ({
                  label: item.name,
                  data: item.byMonth,
                  backgroundColor: [COLOR[index]],
                  borderColor: [BORDER[index]],
                  borderWidth: 1,
                })),
              }}
              options={{
                responsive: true,
                plugins: {
                  legend: {
                    position: "top",
                  },
                  title: {
                    display: true,
                    text: "Biểu đồ mức độ bán của các hãng trong năm",
                  },
                },
              }}
            />
          </div>
        </Grid>
        <Grid
          item
          xs={6}
          style={{ height: "calc((100vh - 60px)/2)" }}
          className="flex justify-center items-center"
        >
          <div className="w-10/12">
            <Bar
              data={{
                labels: Object.values(STATUS),
                datasets: productDataChart.productStatus?.map(
                  (item, index) => ({
                    label: item.name,
                    data: item.byPhase,
                    backgroundColor: [COLOR[index]],
                  })
                ),
              }}
              options={{
                plugins: {
                  title: {
                    display: true,
                    text: "Biểu đồ sản phẩm ứng với trạng thái đơn hàng",
                  },
                },
                responsive: true,
                scales: {
                  x: {
                    stacked: true,
                  },
                  y: {
                    stacked: true,
                  },
                },
              }}
            />
          </div>
        </Grid>
        <Grid
          item
          xs={6}
          style={{ height: "calc((100vh - 60px)/2)" }}
          className="flex justify-center items-center"
        >
          <div className="w-10/12">
            <Line
              data={{
                labels: [...Array(12).keys()].map((item) => `T${item + 1}`),
                datasets: productDataChart.statusSpeed?.map((item, index) => ({
                  label: item.name,
                  data: item.byMonth,
                  backgroundColor: [COLOR[index]],
                  borderColor: [BORDER[index]],
                  borderWidth: 1,
                })),
              }}
              options={{
                responsive: true,
                plugins: {
                  legend: {
                    position: "top",
                  },
                  title: {
                    display: true,
                    text: "Biểu đồ trạng thái và số lượng đơn trong năm",
                  },
                },
              }}
            />
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Analysis;
