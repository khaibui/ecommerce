import React from "react";

import AnalysisGrid from "./AnalysisGrid";

const Analysis = () => {
  return <AnalysisGrid />;
};

export default Analysis;
