import React, { useState, useEffect, useRef } from "react";
import { Button, Grid, Backdrop, CircularProgress } from "@material-ui/core";
import { Classes, Dialog } from "@blueprintjs/core";

import Firebase from "../../../firebase/firebase";
import * as FirebaseService from "../../../firebase/service";
import * as Functions from "../../../utils/CommonFunction";

const Render = [
  {
    key: "cateCode",
    label: "Hãng sản xuất",
    type: "select",
    option: [
      "dell",
      "asus",
      "hp",
      "lenovo",
      "apple",
      "msi",
      "accer",
      "huawei",
      "lg",
    ],
  },
  {
    key: "productTitle",
    label: "Tên sản phẩm",
    type: "input",
  },
  {
    key: "productPrice",
    label: "Giá sản phẩm",
    type: "input",
    isNumber: true,
  },
  {
    key: "productTotal",
    label: "Tổng số lượng",
    type: "input",
    isNumber: true,
  },
  {
    key: "productRemaind",
    label: "Số lượng còn lại",
    type: "input",
    isNumber: true,
  },
  {
    key: "productDiscount",
    label: "Giảm giá",
    type: "input",
    isNumber: true,
  },
  {
    key: "productActive",
    label: "Trạng thái",
    type: "select",
    option: ["active", "disabled"],
  },
  {
    key: "productTitleScript",
    label: "Mô tả",
    type: "input",
  },
];

const RenderDetail = [
  {
    key: "board",
    label: "Mainboard",
  },
  {
    key: "color",
    label: "Màu sắc",
  },
  {
    key: "cpu",
    label: "CPU",
  },
  {
    key: "cpuGen",
    label: "Thế hệ CPU",
  },
  {
    key: "display",
    label: "Màn hình",
  },
  {
    key: "gate",
    label: "Cổng kết nối",
  },
  {
    key: "graphicChip",
    label: "Chip đồ họa",
  },
  {
    key: "graphicGate",
    label: "Cổng đồ họa",
  },
  {
    key: "guarantee",
    label: "Bảo hành",
  },
  {
    key: "led",
    label: "Đèn",
  },
  {
    key: "moreDevice",
    label: "Các cổng khác",
  },
  {
    key: "os",
    label: "Hệ điều hành",
  },
  {
    key: "pin",
    label: "Dung lượng pin",
  },
  {
    key: "ram",
    label: "Dung lượng RAM",
  },
  {
    key: "security",
    label: "Bảo mật",
  },
  {
    key: "series",
    label: "Series",
  },
  {
    key: "size",
    label: "Kích thước",
  },
  {
    key: "store",
    label: "Bộ nhớ",
  },
  {
    key: "weight",
    label: "Cân nặng",
  },
  {
    key: "wireless",
    label: "Wireless",
  },
];

let isUpload = false;

const ProductDialog = (props) => {
  const ref = useRef(null);
  const classes = Functions.useStyles();

  const [data, setData] = useState({});
  const [image, setImage] = useState([]);
  const [originImage, setOriginImage] = useState([]);
  const [url, setUrl] = useState([]);
  const [id, setId] = useState();
  const [backDropOpen, setBackDropOpen] = useState(false);

  const onAddImage = () => ref.current.click();

  const onImageChange = (event) => {
    setOriginImage(event.target.files);
    Array.from(event.target.files).forEach((item, index) => {
      const reader = new FileReader();
      reader.onload = () => {
        if (reader.readyState === 2) {
          if (index === 0) setImage([reader.result]);
          else setImage((prev) => [...prev, reader.result]);
        }
      };
      reader.readAsDataURL(event.target.files[index]);
    });
  };

  const onMainChange = (key, value) => {
    const updateData = { ...data, [key]: value };
    setData(updateData);
  };

  const onSecondaryChange = (key, value) => {
    const updateDetailData = { ...data.productDetail, [key]: value };
    const updateData = { ...data, productDetail: updateDetailData };
    setData(updateData);
  };

  const onConfirmClick = async () => {
    // TODO: validate before save
    const mainValidate = Render.some((item) => !data[item.key]);
    const secondaryValidate = RenderDetail.some(
      (item) => !data.productDetail[item.key]
    );
    const imageValidate = !data.productCode && !originImage.length;
    if (mainValidate || secondaryValidate || imageValidate) {
      console.log("error");
      return;
    }

    setBackDropOpen(true);

    if (!data.productCode) {
      await Firebase.firestore
        .collection("products")
        .add({
          ...data,
        })
        .then(async (doc) => {
          isUpload = true;
          setId(doc.id);
          await FirebaseService.updateOneDataFromStore("products", doc.id, {
            productCode: doc.id,
            ...data,
          });
        });
    } else {
      await FirebaseService.updateOneDataFromStore(
        "products",
        data.productCode,
        { ...data }
      );
      props.handleClose();
      props.reload();
      setBackDropOpen(false);
    }
  };

  useEffect(() => {
    if (originImage.length && id.length && isUpload) {
      image.forEach((img, index) => {
        const uploadTask = Firebase.storage
          .ref(`${data.cateCode}/${id}/${index}`)
          .put(originImage[index]);
        uploadTask.on(
          "state_changed",
          (snapshot) => {},
          (error) => {},
          () => {
            uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
              setUrl((prev) => [...prev, downloadURL]);
            });
          }
        );
      });
      isUpload = false;
    }
  }, [id]);

  useEffect(() => {
    if (url.length && url.length === image.length && id.length) {
      FirebaseService.updateOneDataFromStore("products", id, {
        image: url,
      }).then((doc) => {
        setOriginImage([]);
        setUrl([]);
        setId("");
        props.handleClose();
        props.reload();
        setBackDropOpen(false);
      });
    }
  }, [url]);

  useEffect(() => {
    if (Object.keys(props.data).length) {
      setData(props.data);
      setImage(props.data.image);
      setOriginImage([]);
    } else {
      setData({
        cateCode: "dell",
        productActive: "active",
        productDetail: {},
      });
      setImage([]);
      setOriginImage([]);
    }
  }, [props.data]);

  return (
    <React.Fragment>
      <Backdrop className={classes.backdrop} open={backDropOpen}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Dialog
        isOpen={props.open}
        onClose={props.handleClose}
        icon="projects"
        title="Thông tin sản phẩm"
        style={{ width: 1000, marginTop: 60 }}
      >
        <div className={Classes.DIALOG_BODY}>
          <div className="w-full flex flex-row overflow-x-hidden">
            <div className="w-6/12 flex flex-col">
              <div className="w-full flex">
                <Button className="my-3" onClick={onAddImage}>
                  Chọn ảnh
                </Button>
              </div>

              <Grid container spacing={2} margin={2} className="mb-5">
                {image.map((item) => (
                  <Grid item xs={3} className="border rounded-lg">
                    <img className="w-full h-auto" src={item} />
                  </Grid>
                ))}
              </Grid>

              {Render.map((item) =>
                item.type === "input" ? (
                  <label class="bp3-label ">
                    {item.label}
                    <input
                      class="bp3-input bp3-fill"
                      dir="auto"
                      type={item.isNumber ? "number" : "text"}
                      value={data[item.key]}
                      onChange={(e) => onMainChange(item.key, e.target.value)}
                    />
                  </label>
                ) : (
                  <label class="bp3-label">
                    {item.label}
                    <div class="bp3-select bp3-fill">
                      <select
                        value={data[item.key]}
                        onChange={(e) => onMainChange(item.key, e.target.value)}
                      >
                        {item.option.map((option) => (
                          <option value={option}>
                            {option.charAt(0).toUpperCase() + option.slice(1)}
                          </option>
                        ))}
                      </select>
                    </div>
                  </label>
                )
              )}

              <input
                multiple
                type="file"
                className="hidden"
                ref={ref}
                accept="image/*"
                onChange={onImageChange}
                onClick={(e) => {
                  e.target.value = "";
                }}
              />
            </div>

            <div className="w-6/12 flex flex-col pl-5">
              {RenderDetail.map((item) => (
                <label class="bp3-label ">
                  {item.label}
                  <input
                    class="bp3-input bp3-fill"
                    dir="auto"
                    value={data.productDetail && data.productDetail[item.key]}
                    onChange={(e) =>
                      onSecondaryChange(item.key, e.target.value)
                    }
                  />
                </label>
              ))}
            </div>
          </div>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button onClick={props.handleClose} color="default">
              Bỏ qua
            </Button>
            <Button onClick={onConfirmClick}>Xác nhận</Button>
          </div>
        </div>
      </Dialog>
    </React.Fragment>
  );
};

export default ProductDialog;
