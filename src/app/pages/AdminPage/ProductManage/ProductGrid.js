import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Grid,
  Button,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Tooltip,
  Backdrop,
  CircularProgress,
} from "@material-ui/core";

import { filterEngine } from "../../../utils/Constraint";

import ProductDialog from "./ProductDialog";

import * as FirebaseService from "../../../firebase/service";
import * as Actions from "../../../utils/store/action";
import * as Functions from "../../../utils/CommonFunction";

const ProductGrid = (props) => {
  const dispatch = useDispatch();
  const classes = Functions.useStyles();

  const products = useSelector(({ User }) => User.User.products);

  const [backDropOpen, setBackDropOpen] = useState(false);
  const [reload, setReload] = useState(false);
  const [selected, setSelected] = useState({});
  const [openDialog, setOpenDialog] = useState(false);
  const [filter, setFilter] = useState({
    active: "active",
    cate: "all",
  });

  const onEditClick = (data) => {
    setSelected(data);
    setOpenDialog(true);
  };

  const onAddClick = () => {
    setSelected({});
    setOpenDialog(true);
  };

  const onCloseDialog = () => {
    setOpenDialog(false);
    setSelected({});
  };

  useEffect(async () => {
    setBackDropOpen(true);
    await FirebaseService.getALlDataFromStore("products").then(
      async (snapshot) => {
        const dataSnapshot = snapshot.docs.map((item) => ({ ...item.data() }));
        console.log(dataSnapshot);
        dispatch(Actions.setProduct(dataSnapshot));
      }
    );
    setBackDropOpen(false);
  }, [reload]);

  return (
    <React.Fragment>
      <Backdrop className={classes.backdrop} open={backDropOpen}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <div
        className="w-full px-3 absolute top-14 overflow-y-auto"
        style={{ height: "calc(100vh - 120px)" }}
      >
        <Grid container spacing={3}>
          {products
            .filter((item) => {
              const byActive =
                filter.active === "all" || filter.active === item.productActive;
              const byCate =
                filter.cate === "all" || filter.cate === item.cateCode;
              return byActive && byCate;
            })
            .map((item) => (
              <Grid item xs={2}>
                <Card className="w-full" onClick={() => onEditClick(item)}>
                  <CardActionArea>
                    <CardMedia
                      className="h-48"
                      image={item.image[0]}
                      title="Contemplative Reptile"
                    />
                    <CardContent>
                      <Tooltip title={item.productTitle}>
                        <div className="text-md font-bold truncate">
                          {item.productTitle}
                        </div>
                      </Tooltip>
                      <div className="mt-2">{item.productPrice} VND</div>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
        </Grid>
      </div>

      <div className="w-full absolute min-h-20 p-3 bottom-0 flex items-end">
        <div className="w-2/12 flex">
          <Button onClick={onAddClick}>Thêm sản phẩm</Button>
        </div>
        <div className="w-10/12 flex flex-row justify-end	items-end">
          <div className="w-2/12 mr-3">
            <label class="bp3-label">
              Trạng thái sản phẩm
              <div class="bp3-select bp3-fill">
                <select
                  onChange={(e) =>
                    setFilter((pre) => ({ ...pre, active: e.target.value }))
                  }
                  value={filter.active}
                >
                  <option value="all">Tất cả</option>
                  <option value="active">Active</option>
                  <option value="disabled">Disabled</option>
                </select>
              </div>
            </label>
          </div>

          <div className="w-2/12">
            <label class="bp3-label">
              Thương hiệu
              <div class="bp3-select bp3-fill">
                <select
                  onChange={(e) =>
                    setFilter((pre) => ({ ...pre, cate: e.target.value }))
                  }
                  value={filter.cate}
                >
                  <option value="all">Tất cả</option>
                  {filterEngine[0].detail.map((item) => (
                    <option value={item.code}>{item.name}</option>
                  ))}
                </select>
              </div>
            </label>
          </div>
        </div>
      </div>

      <ProductDialog
        open={openDialog}
        data={selected}
        handleClose={onCloseDialog}
        reload={() => setReload(!reload)}
      />
    </React.Fragment>
  );
};

export default ProductGrid;
