import React, { useState, useEffect } from "react";
import {
  Classes,
  Dialog,
  Menu,
  MenuDivider,
  MenuItem,
} from "@blueprintjs/core";
import { Button, Backdrop, CircularProgress } from "@material-ui/core";

import * as FirebaseService from "../../../firebase/service";
import * as Functions from "../../../utils/CommonFunction";

const ProductItem = (props) => {
  return (
    <div className="user-payment-item-container">
      <img className="user-payment-item-image" src={props.data.image[0]} />
      <div className="user-payment-item-name">{props.data.productTitle}</div>
      <div className="user-payment-item-counter">{props.data.productTotal}</div>
      <div className="user-payment-item-price">
        {`${(
          props.data.productTotal * props.data.productFinalPrice
        ).toLocaleString()}đ`}
      </div>
    </div>
  );
};

const ProcessDialog = (props) => {
  const classes = Functions.useStyles();

  const [backDropOpen, setBackDropOpen] = useState(false);
  const [dialogData, setDialogData] = useState({});
  const [canSave, setCanSave] = useState(false);
  const [status, setStatus] = useState("");
  const [payment, setPayment] = useState("");

  const onStatusChange = (e) => {
    setStatus(e.target.value);
    setCanSave(true);
  };

  const onPaymentChange = (e) => {
    setPayment(e.target.value);
    setCanSave(true);
  };

  const onCancelClick = () => {
    props.onClose();
    setCanSave(false);
    setStatus("");
    setPayment("");
  };

  const onConfirmClick = async () => {
    if (dialogData && Object.keys(dialogData)) {
      setBackDropOpen(true);
      const updateBill = dialogData.userBill.map((item) =>
        item.billCode === dialogData.billCode
          ? { ...item, billStatus: status, billPayment: payment }
          : { ...item }
      );
      await FirebaseService.updateOneDataFromStore("users", dialogData.userId, {
        userBill: updateBill,
      });
      onCancelClick();
      setBackDropOpen(false);
      props.reload();
    }
  };

  useEffect(() => {
    if (props.data && Object.keys(props.data).length) {
      setDialogData(props.data);
      setStatus(props.data.billStatus);
      setPayment(props.data.billPayment);
      console.log(props.data);
    }
  }, [props.isOpen]);

  return (
    <React.Fragment>
      <Backdrop className={classes.backdrop} open={backDropOpen}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Dialog
        {...props}
        onClose={onCancelClick}
        icon="list-detail-view"
        title="Chi tiết đơn hàng"
        style={{ maxWidth: 1000, marginTop: 60 }}
      >
        <div className={Classes.DIALOG_BODY}>
          <Menu
            large
            minimal
            tagName="abc"
            className="user-payment-menu-container"
          >
            <MenuItem
              large
              icon="dollar"
              text="Giá trị đơn hàng"
              label={`${dialogData.billTotal?.toLocaleString()}đ` || ""}
            />
            <MenuItem
              icon="mobile-phone"
              text="Điện thoại nhận hàng"
              label={dialogData.userProfile?.userPhone || ""}
            />
            <MenuItem
              icon="map-marker"
              text="Địa chỉ nhận hàng"
              label={dialogData.userProfile?.userAddress || ""}
            />
            <MenuItem
              icon="user"
              text="Tên khách hàng"
              label={dialogData.userProfile?.userFullName || ""}
            />
            <MenuItem
              icon="calendar"
              text="Ngày đặt hàng"
              label={dialogData.billCreatedAt || ""}
            />
            <MenuItem
              icon="intersection"
              text="Hình thức thanh toán"
              label={
                dialogData.billPaymentType === "zalo"
                  ? "Thanh toán ZaloPay"
                  : "Thanh toán khi nhận hàng"
              }
            />
            <MenuDivider />
          </Menu>
          <Menu
            large
            minimal
            tagName="abc"
            className="user-payment-menu-container"
          >
            <label class="bp3-label">
              Trạng thái đơn hàng
              <div class="bp3-select bp3-fill">
                <select onChange={onStatusChange} value={status}>
                  <option value="wait">Chờ xác nhận</option>
                  <option value="confirm">Đã xác nhận - chờ vận chuyển</option>
                  <option value="tranfer">Đang vận chuyển</option>
                  <option value="success">Đơn thành công</option>
                  <option value="cancel">Đơn đã hủy</option>
                </select>
              </div>
            </label>
            <label class="bp3-label">
              Trạng thái thanh toán
              <div class="bp3-select bp3-fill">
                <select
                  onChange={onPaymentChange}
                  disabled={dialogData.billPaymentType === "zalo"}
                  value={
                    dialogData.billPaymentType === "zalo"
                      ? "yes"
                      : payment || "no"
                  }
                >
                  <option value="no">Chưa thanh toán</option>
                  <option value="yes">Đã thanh toán</option>
                </select>
              </div>
            </label>
            <MenuDivider />
          </Menu>
          <Menu
            large
            minimal
            tagName="abc"
            className="user-payment-menu-container"
          >
            {dialogData.billProduct?.map((item) => (
              <ProductItem data={item} />
            ))}
          </Menu>
        </div>
        <div className={Classes.DIALOG_FOOTER}>
          <div className={Classes.DIALOG_FOOTER_ACTIONS}>
            <Button onClick={onCancelClick} color="default">
              Bỏ qua
            </Button>
            <Button disabled={!canSave} onClick={onConfirmClick}>
              Xác nhận
            </Button>
          </div>
        </div>
      </Dialog>
    </React.Fragment>
  );
};

export default ProcessDialog;
