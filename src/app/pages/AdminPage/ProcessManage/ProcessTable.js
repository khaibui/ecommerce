import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TableHead,
  Button,
  Tooltip,
  Backdrop,
  CircularProgress,
} from "@material-ui/core";
import { CheckCircleOutlined, HighlightOffOutlined } from "@material-ui/icons";

import ProcessDialog from "./ProcessDialog";

import * as FirebaseService from "../../../firebase/service";
import * as Actions from "../../../utils/store/action";
import * as Functions from "../../../utils/CommonFunction";
import { ADMIN_ACCOUNT } from "../../../utils/Constraint";

const BILL_STATUS = ["wait", "confirm", "tranfer", "success", "cancel"];

const headerConstraint = [
  {
    id: "code",
    label: "Mã",
  },
  {
    id: "email",
    label: "Địa chỉ email",
  },
  {
    id: "phone",
    label: "Số điện thoại",
  },
  {
    id: "price",
    label: "Giá tiền",
  },
  {
    id: "payment",
    label: "Đã thanh toán",
  },
  {
    id: "confirm",
    label: "Xác nhận",
  },
  {
    id: "transform",
    label: "Vận chuyển",
  },
  {
    id: "success",
    label: "Nhận hàng",
  },
  {
    id: "cancel",
    label: "Hủy đơn",
  },
  {
    id: "date",
    label: "Ngày đặt",
  },
];

const ProcessTable = (props) => {
  const dispatch = useDispatch();
  const classes = Functions.useStyles();

  const allUsers = useSelector(({ User }) => User.User.allUsers);

  const [backDropOpen, setBackDropOpen] = useState(false);
  const [openDialog, setOpenDialog] = useState(false);
  const [selected, setSelected] = useState(null);
  const [reload, setReload] = useState(false);
  const [billdRenderData, setBillRenderData] = useState([]);

  const onPreClick = async () => {
    const updateLabelIndex = BILL_STATUS.indexOf(selected.billStatus);

    if (selected && updateLabelIndex > 0) {
      setBackDropOpen(true);
      const updateLabel = BILL_STATUS[updateLabelIndex - 1];
      const userData = allUsers.find((item) => item.userId === selected.userId);
      const updateBill = userData.userBill.map((item) =>
        item.billCode === selected.billCode
          ? { ...item, billStatus: updateLabel }
          : { ...item }
      );
      await FirebaseService.updateOneDataFromStore("users", selected.userId, {
        userBill: updateBill,
      });
      setBackDropOpen(false);
      setReload(!reload);
    }
  };

  const onNextClick = async () => {
    const updateLabelIndex = BILL_STATUS.indexOf(selected.billStatus);

    if (selected && updateLabelIndex < 3) {
      setBackDropOpen(true);
      const updateLabel = BILL_STATUS[updateLabelIndex + 1];
      const userData = allUsers.find((item) => item.userId === selected.userId);
      const updateBill = userData.userBill.map((item) =>
        item.billCode === selected.billCode
          ? { ...item, billStatus: updateLabel }
          : { ...item }
      );
      await FirebaseService.updateOneDataFromStore("users", selected.userId, {
        userBill: updateBill,
      });
      setBackDropOpen(false);
      setReload(!reload);
    }
  };

  const onCancelClick = async () => {
    if (selected) {
      setBackDropOpen(true);
      const userData = allUsers.find((item) => item.userId === selected.userId);
      const updateBill = userData.userBill.map((item) =>
        item.billCode === selected.billCode
          ? { ...item, billStatus: "cancel" }
          : { ...item }
      );
      await FirebaseService.updateOneDataFromStore("users", selected.userId, {
        userBill: updateBill,
      });
      setBackDropOpen(false);
      setReload(!reload);
    }
  };

  const onDetailClick = () => {
    setOpenDialog(true);
  };

  useEffect(async () => {
    setBackDropOpen(true);
    await FirebaseService.getALlDataFromStore("users").then(
      async (snapshot) => {
        const dataSnapshot = snapshot.docs.map((item) => ({ ...item.data() }));
        dispatch(Actions.setAllUserAccount(dataSnapshot));
      }
    );
    setBackDropOpen(false);
  }, [reload]);

  useEffect(() => {
    if (allUsers && allUsers.length) {
      const billData = allUsers.flatMap((user) => {
        const hadBill =
          user.userBill &&
          user.userBill.length &&
          !ADMIN_ACCOUNT.includes(user.userEmail);
        return hadBill
          ? user.userBill.map((bill) => ({ ...user, ...bill }))
          : [];
      });
      setBillRenderData(billData);
    }
  }, [allUsers]);

  return (
    <React.Fragment>
      <ProcessDialog
        isOpen={openDialog}
        onClose={() => setOpenDialog(false)}
        data={selected}
        reload={() => setReload(!reload)}
      />
      <Backdrop className={classes.backdrop} open={backDropOpen}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <div
        className="w-full absolute top-14 overflow-auto"
        style={{ maxHeight: "calc(100vh - 120px)" }}
      >
        <Table
          stickyHeader
          className="w-full cursor-pointer h-full"
          size="small"
        >
          <TableHead>
            <TableRow>
              {headerConstraint.map((headerItem) => (
                <TableCell
                  key={headerItem.id}
                  className="text-left table-cell "
                >
                  <div className="w-full flex flex-row items-center">
                    {headerItem.label}
                  </div>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {billdRenderData.map((item) => (
              <TableRow
                hover
                selected={selected && item.billCode === selected.billCode}
                onClick={() => setSelected(item)}
              >
                <TableCell className="w-1/12">
                  <Tooltip title={item.billCode}>
                    <div className="truncate w-24">{item.billCode}</div>
                  </Tooltip>
                </TableCell>
                <TableCell className="w-1/12">
                  <Tooltip title={item.userEmail}>
                    <div className="truncate w-24">{item.userEmail}</div>
                  </Tooltip>
                </TableCell>
                <TableCell className="w-1/12">
                  <Tooltip title={item.userProfile.userPhone}>
                    <div className="truncate w-24">
                      {item.userProfile.userPhone}
                    </div>
                  </Tooltip>
                </TableCell>
                <TableCell className="w-1/12">
                  <Tooltip title={item.billTotal}>
                    <div className="truncate w-24">{`${item.billTotal.toLocaleString()}đ`}</div>
                  </Tooltip>
                </TableCell>
                <TableCell className="w-1/12">
                  {item.billPayment === "yes" && (
                    <CheckCircleOutlined
                      fontSize="small"
                      style={{ fill: "#53a653" }}
                    />
                  )}
                </TableCell>
                <TableCell className="w-1/12">
                  {["confirm", "success", "tranfer"].includes(
                    item.billStatus
                  ) && (
                    <CheckCircleOutlined
                      fontSize="small"
                      style={{ fill: "#53a653" }}
                    />
                  )}
                </TableCell>
                <TableCell className="w-1/12">
                  {["success", "tranfer"].includes(item.billStatus) && (
                    <CheckCircleOutlined
                      fontSize="small"
                      style={{ fill: "#53a653" }}
                    />
                  )}
                </TableCell>
                <TableCell className="w-1/12">
                  {item.billStatus === "success" && (
                    <CheckCircleOutlined
                      fontSize="small"
                      style={{ fill: "#53a653" }}
                    />
                  )}
                </TableCell>
                <TableCell className="w-1/12">
                  {item.billStatus === "cancel" && (
                    <HighlightOffOutlined
                      fontSize="small"
                      style={{ fill: "#ee4337" }}
                    />
                  )}
                </TableCell>
                <TableCell className="w-1/12">
                  <Tooltip title={item.billCreatedAt}>
                    <div className="truncate w-24">{item.billCreatedAt}</div>
                  </Tooltip>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>

      <div className="w-full absolute h-16 p-3 bottom-0 flex items-center">
        <Button disabled={!selected} onClick={onDetailClick}>
          Chi tiết
        </Button>
        <Button
          disabled={!selected || selected.billStatus === "cancel"}
          onClick={onPreClick}
        >
          Giai đoạn trước
        </Button>
        <Button
          disabled={!selected || selected.billStatus === "cancel"}
          onClick={onNextClick}
        >
          Giai đoạn kế
        </Button>
        <Button
          disabled={!selected || selected.billStatus === "cancel"}
          onClick={onCancelClick}
        >
          Hủy đơn hàng
        </Button>
      </div>
    </React.Fragment>
  );
};

export default ProcessTable;
