import Firebase from "./firebase";

export const getAllImage = async (colParam, docParam) => {
  const storageRef = Firebase.storage.ref();
  const imageRefs = await storageRef.child(`${colParam}/${docParam}`).listAll();
  const imageURLs = [];
  for (let i = 0; i < imageRefs.items.length; i++) {
    const imageURL = await imageRefs.items[i].getDownloadURL();
    imageURLs.push(imageURL);
  }
  return imageURLs;
};

export const getOneDataFromStore = async (colParam, docParam) => {
  const firestore = Firebase.firestore.collection(colParam).doc(docParam);
  return await firestore.get().catch((err) => console.log(err));
};

export const getALlDataFromStore = async (colParam) => {
  const firestore = Firebase.firestore.collection(colParam);
  return await firestore.get().catch((err) => console.log(err));
};

export const updateOneDataFromStore = async (
  colParam,
  docParam,
  fieldParam
) => {
  const firestore = Firebase.firestore.collection(colParam).doc(docParam);
  return await firestore.update(fieldParam);
};

export const addDataToStore = async (colParam, docParam, fieldParam) => {
  const firestore = Firebase.firestore.collection(colParam);
  return await firestore
    .doc(docParam)
    .set({
      ...fieldParam,
      createdAt: new Date().getTime(),
    })
    .catch((err) => console.log(err));
};

export const createUserToStore = async (colParam, docParam, fieldParam) => {
  return await addDataToStore(colParam, docParam, fieldParam);
};

export const createUserByEmailPassword = async (emailParam, passwordParam) => {
  const auth = Firebase.auth;
  return await auth.createUserWithEmailAndPassword(emailParam, passwordParam);
};

export const signInByEmailPassword = async (emailParam, passwordParam) => {
  const auth = Firebase.auth;
  return await auth.signInWithEmailAndPassword(emailParam, passwordParam);
};

export const signOut = () => {
  const auth = Firebase.auth;
  return auth.signOut().catch((err) => console.log(err));
};
