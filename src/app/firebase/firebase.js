import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyB7hcaQbeMdgqHAT4MeFi5lWCgSPZW17E8",
  authDomain: "ecommerce-efcd0.firebaseapp.com",
  databaseURL: "https://ecommerce-efcd0-default-rtdb.firebaseio.com",
  projectId: "ecommerce-efcd0",
  storageBucket: "ecommerce-efcd0.appspot.com",
  messagingSenderId: "88847794887",
  appId: "1:88847794887:web:2cbb931a1f787296bab4e2",
  measurementId: "G-SZGDVEQ6EN",
};

firebase.initializeApp(firebaseConfig);
export default {
  firestore: firebase.firestore(),
  storage: firebase.storage(),
  auth: firebase.auth(),
};
